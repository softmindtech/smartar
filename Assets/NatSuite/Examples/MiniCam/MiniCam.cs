/* 
*   NatDevice
*   Copyright (c) 2020 Yusuf Olokoba.
*/

namespace NatSuite.Examples {
    
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;
    using System.Threading.Tasks;
    using Devices;
    using static Devices.MediaDeviceQuery;

    public class MiniCam : MonoBehaviour {
        
        [Header("Camera Preview")]
        public RawImage previewPanel;
        public AspectRatioFitter previewAspectFitter;

        [Header("Photo Capture")]
        public RawImage photoPanel;
        public AspectRatioFitter photoAspectFitter;
        public Image flashIcon;
        public Image switchIcon;
        public Image blockImage;
        MediaDeviceQuery query;
        public switchCamera m_switchCam;

        #region --Setup--

        async void Start () {
            //Request camera permissions
            if (!await MediaDeviceQuery.RequestPermissions<CameraDevice>()) {
                Debug.LogError("User did not grant camera permissions");
                return;
            }
            // Create a device query for device cameras
            // Use `GenericCameraDevice` so we also capture WebCamTexture cameras

            
            previewPanel.gameObject.SetActive(false);
            // // Start camera preview
            // var device = query.currentDevice as ICameraDevice;
            // var previewTexture = await device.StartRunning();
            // Debug.Log($"Started camera preview with resolution {previewTexture.width}x{previewTexture.height}");
            // // Display preview texture
            // previewPanel.texture = previewTexture;
            // previewAspectFitter.aspectRatio = previewTexture.width / (float)previewTexture.height;
            // previewPanel.color = new Color(1,1,1,0);
            // Set UI state
            //switchIcon.color = query.devices.Length > 1 ? Color.white : Color.gray;
            //flashIcon.color = device is CameraDevice cameraDevice && cameraDevice.flashSupported ? Color.white : Color.gray;
        }


        void OnApplicationFocus( bool focus )
        {
            if( focus )
            {

                if(!m_switchCam.cameraSwicth)
                    return;

                AutoRotateChange();
            }
        }
        #endregion


        #region --UI Delegates--

        public async void CapturePhoto () {
            // Only `CameraDevice` supports capturing photos, not `ICameraDevice`
            if (query.currentDevice is CameraDevice device) {
                // Capture photo
                var photoTexture = await device.CapturePhoto();
                Debug.Log($"Captured photo with resolution {photoTexture.width}x{photoTexture.height}");
                // Display photo texture for a few seconds
                photoPanel.gameObject.SetActive(true);
                photoPanel.texture = photoTexture;
                photoAspectFitter.aspectRatio = photoTexture.width / (float)photoTexture.height;
                await Task.Delay(3_000);
                // Restore preview and destroy photo
                photoPanel.gameObject.SetActive(false);
                Texture2D.Destroy(photoTexture);
            }
        }

        public async void SwitchFontCamera (bool value) {
            // Check that there is another camera to switch to

            if(value)
            {
                query = new MediaDeviceQuery(Criteria.GenericCameraDevice);

                if (query.devices.Length < 2)
                    return;

                query.Advance();

                previewPanel.gameObject.SetActive(true);
                query = new MediaDeviceQuery(Criteria.GenericCameraDevice);
                query.Advance();
                
                var cameraDevice = query.currentDevice as ICameraDevice;
                var previewTexture = await cameraDevice.StartRunning();
                // Display preview texture
                
                previewAspectFitter.aspectRatio = previewTexture.width / (float)previewTexture.height;
                previewPanel.texture = previewTexture;

            }
            else
            {
                previewPanel.gameObject.SetActive(false);
                var cameraDevice = query.currentDevice as ICameraDevice;
                StopCamera();

            }
        }
        async public void AutoRotateChange()
        {
            blockImage.enabled = true;
            SwitchFontCamera(false);
            
            await Task.Delay(500);
            blockImage.enabled = false;
            SwitchFontCamera(true);
        }

        public void StopCamera()
        {
            previewPanel.gameObject.SetActive(false);
            if(query == null)
                return;

            var cameraDevice = query.currentDevice as ICameraDevice;
            cameraDevice.StopRunning();
        }
        public void FocusCamera (BaseEventData e) {
            // Only `CameraDevice` supports setting focus point, not `ICameraDevice`
            if (query == null || query.devices.Length == 0)
                return;
            
            if (query.currentDevice is CameraDevice device) {
                // Get the touch position in viewport coordinates
                var eventData = e as PointerEventData;
                RectTransform transform = eventData.pointerPress.GetComponent<RectTransform>();
                if (!RectTransformUtility.ScreenPointToWorldPointInRectangle(transform, eventData.pressPosition, eventData.pressEventCamera, out var worldPoint))
                    return;
                var corners = new Vector3[4];
                transform.GetWorldCorners(corners);
                var point = worldPoint - corners[0];
                var size = new Vector2(corners[3].x, corners[1].y) - (Vector2)corners[0];
                // Focus camera at point
                device.focusPoint = (point.x / size.x, point.y / size.y);
            }
        }

        public void ToggleFlashMode () {
            // Only `CameraDevice` supports setting focus point, not `ICameraDevice`
            if (query.currentDevice is CameraDevice device) {
                device.flashMode = device.flashMode == FlashMode.On ? FlashMode.Off : FlashMode.On;
                flashIcon.color = device.flashMode == FlashMode.On ? Color.white : Color.gray;
            }
        }
        #endregion
    }
}