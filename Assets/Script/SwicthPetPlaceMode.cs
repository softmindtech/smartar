﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class SwicthPetPlaceMode : MonoBehaviour
{

    public Sprite[] m_sprite;
    public switchCamera m_switchCamera;
    Image m_image;
    Button m_btn;
    public Button CameraViewBtn;

    public bool ARmode {get; private set;} = true;
    // Start is called before the first frame update
    void Start()
    {
        m_image = GetComponent<Image>();
        m_btn = GetComponent<Button>();
        m_image.sprite = m_sprite[0];
        m_btn.onClick.AddListener(switchPlaceMode);
        m_switchCamera.m_placemode = (switchCamera.placemode)1;
        StartCoroutine(DelayAction());
    }
    // Update is called once per frame
    void switchPlaceMode()
    {
        ARmode = !ARmode;
        int mode = ARmode == true ? 1 : 0;
        CameraViewBtn.interactable = !ARmode;
        m_image.sprite = m_sprite[mode == 1 ? 0 : 1];
        m_switchCamera.m_placemode = (switchCamera.placemode)mode;
        m_switchCamera.changePlaceMode();
    }

    IEnumerator DelayAction()
    {
        yield return new WaitForSeconds(0.5f);
        switchPlaceMode();
    }
}
