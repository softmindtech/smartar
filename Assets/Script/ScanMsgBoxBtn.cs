﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanMsgBoxBtn : MonoBehaviour
{
    Button m_btn;
    public GameObject ScanMsgBox;
    public GameObject ScanTip;
    public MarkerManagement markerCtr;
    public GameObject ReverseMaskLayer;
    
    public GameObject ReverseMask;
    public GameObject ImageMask;

    void Start()
    {
        ScanTip.SetActive(false);
        m_btn = GetComponent<Button>();
        m_btn.onClick.AddListener(OnButtonPress);

        ImageMask.SetActive(false);
    }

    void OnButtonPress()
    {
        ScanTip.SetActive(true);
        //ReverseMaskLayer.SetActive(true);
        //ReverseMask.SetActive(true);
        //
        //other function
        //
        ScanMsgBox.SetActive(false);

        markerCtr.beginCheck = true;

        ImageMask.SetActive(true);
    }
}
