﻿using System;
using System.Collections;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class MarkerManagement : MonoBehaviour
{
    public GameObject resultBox;
    public GameObject desEng;
    public GameObject desChi;

    public Text resultMsg;
    public Text btnTxt;
    public ApiController api;
    public LocationService locationService;
    public bool beginCheck = false;

    private int intervalLimit = 2;
    private bool isSamePlace = false;
    private bool msgShown = false;
    private bool forAll = false;
    private bool processingAPI = false;

    [SerializeField]
    [Tooltip("Image manager on the AR Session Origin")]
    ARTrackedImageManager m_ImageManager;

    /// <summary>
    /// Get the <c>ARTrackedImageManager</c>
    /// </summary>
    public ARTrackedImageManager ImageManager
    {
        get => m_ImageManager;
        set => m_ImageManager = value;
    }

    [SerializeField]
    [Tooltip("Reference Image Library")]
    XRReferenceImageLibrary m_ImageLibrary;

    /// <summary>
    /// Get the <c>XRReferenceImageLibrary</c>
    /// </summary>
    public XRReferenceImageLibrary ImageLibrary
    {
        get => m_ImageLibrary;
        set => m_ImageLibrary = value;
    }

    [SerializeField]
    [Tooltip("Prefab for tracked 1 image")]
    GameObject m_OnePrefab;

    /// <summary>
    /// Get the one prefab
    /// </summary>
    public GameObject onePrefab
    {
        get => m_OnePrefab;
        set => m_OnePrefab = value;
    }

    GameObject m_SpawnedOnePrefab;

    /// <summary>
    /// get the spawned one prefab
    /// </summary>
    public GameObject spawnedOnePrefab
    {
        get => m_SpawnedOnePrefab;
        set => m_SpawnedOnePrefab = value;
    }

    [SerializeField]
    [Tooltip("Prefab for tracked 2 image")]
    GameObject m_TwoPrefab;

    /// <summary>
    /// get the two prefab
    /// </summary>
    public GameObject twoPrefab
    {
        get => m_TwoPrefab;
        set => m_TwoPrefab = value;
    }

    GameObject m_SpawnedTwoPrefab;

    /// <summary>
    /// get the spawned two prefab
    /// </summary>
    public GameObject spawnedTwoPrefab
    {
        get => m_SpawnedTwoPrefab;
        set => m_SpawnedTwoPrefab = value;
    }

    int m_NumberOfTrackedImages;

    NumberManager m_OneNumberManager;
    NumberManager m_TwoNumberManager;

    static Guid s_FirstImageGUID;
    static Guid s_SecondImageGUID;

    void OnEnable()
    {
        int language = PlayerPrefs.GetInt("Language");
        if (language == 0)
        {
            desEng.SetActive(true);
            desChi.SetActive(false);
        }
        else
        {
            desEng.SetActive(false);
            desChi.SetActive(true);
        }
        s_FirstImageGUID = m_ImageLibrary[0].guid;
        s_SecondImageGUID = m_ImageLibrary[1].guid;

        m_ImageManager.trackedImagesChanged += ImageManagerOnTrackedImagesChanged;

        isSamePlace = locationService.CheckSamePlace();
        resultBox.SetActive(false);

        //  UnlockCharacter();
    }

    void OnDisable()
    {
        m_ImageManager.trackedImagesChanged -= ImageManagerOnTrackedImagesChanged;
    }

    void ImageManagerOnTrackedImagesChanged(ARTrackedImagesChangedEventArgs obj)
    {

        // locationService.TrackLocaton();
        // updated, set prefab position and rotation
        foreach (ARTrackedImage image in obj.updated)
        {
            // image is tracking or tracking with limited state, show visuals and update it's position and rotation
            int place = PlayerPrefs.GetInt("Place");
          //  Debug.Log("PlaceTom " + place.ToString());
          //  Debug.Log("Index " + PlayerPrefs.GetInt("Index").ToString());
            if (image.trackingState == TrackingState.Tracking && beginCheck)
            {

                for (int i = 0; i < m_ImageLibrary.count; i++)
                {

                    if (image.referenceImage.guid == m_ImageLibrary[i].guid && place >= 0)
                    {
                        if (!processingAPI)
                        {
                            processingAPI = true;
                            if (m_ImageLibrary[i].name.Contains("super"))
                            {
                               // UnLockCheck2(m_ImageLibrary[i].name);
                            }
                         //   else if (PlayerPrefs.GetInt("Index")<10)
                          else
                            {
                                api.GetRedeemCode(() =>
                                {

                                //  resultMsg.text = m_ImageLibrary[i].name;

                                if (place < 25 && m_ImageLibrary[i].name.Contains("old"))
                                    {
                                        UnLockCheck(i);
                                    }

                                    if (place >= 25 && place < 33 && m_ImageLibrary[i].name.Contains("new"))
                                    {
                                        UnLockCheck(i);
                                    }

                                    if (place >= 33 && place < 9999 && m_ImageLibrary[i].name.Contains("ifc"))
                                    {
                                        UnLockCheck(i);
                                    }

                                    if (m_ImageLibrary[i].name.Contains("all"))
                                    {
                                        forAll = true;
                                        UnLockCheck(i);
                                    }
                                 
                                });
                            }
                        }
                        break;
                    }

                }
               

            }

        }


    }

    public int NumberOfTrackedImages()
    {
        m_NumberOfTrackedImages = 0;
        foreach (ARTrackedImage image in m_ImageManager.trackables)
        {
            if (image.trackingState == TrackingState.Tracking)
            {
                m_NumberOfTrackedImages++;
            }
        }
        return m_NumberOfTrackedImages;
    }

    private void UnLockCheck2(string markerName)
    {
        bool unlock = false;
        int index = PlayerPrefs.GetInt("Index");
      

        if (index < 0)
        {
            //from bottom menu scan btn
            switch (markerName)
            {
                case "super1":
                    index = 10;
                    break;
                case "super2":
                    index = 11;
                    break;
                case "super3":
                    index = 12;
                    break;
                default:
      
                    break;
            }

            unlock = true;
        }
        else
        {
            //from card

            if (markerName == "super1" && index == 10)
                unlock = true;

            if (markerName == "super2" && index == 11)
                unlock = true;

            if (markerName == "super3" && index == 12)
                unlock = true;
        }

        if (unlock)
        {
            api.UploadCharacter((bool succeedFlag) => {
                int language = PlayerPrefs.GetInt("Language");
                if (succeedFlag)
                {
                    if (language == 0)
                    {
                        StartCoroutine(ShowMsgBox("You have successfully unlocked an “Arena of Valor” hero!"));
                        btnTxt.text = "Next";
                    }
                    else
                    {
                        StartCoroutine(ShowMsgBox("你已成功捕捉到《傳說對決》英雄 ！"));
                        btnTxt.text = "下一步";
                    }

                    SelectPetsManager.instance.selectedCategory = 1;
                    SelectPetsManager.instance.scanned = true;
                    SelectPetsManager.instance.selectedPetindex = index;
                    string key = index.ToString() + "lock";
                    PlayerPrefs.SetInt(key, 1);

                    string date = "";
                    NotificationXML.instance.AddData(index, date);
                    NotificationXML.instance.AddData("aaaa", " ");
                }
                else
                {
                    processingAPI = false;
                }

            }, index);
        }
    }

    private void UnLockCheck(int index)
    {
        //UnlockCharacter();

        string key = "MarkerTime" + index.ToString();
        int timestamp = 0;

        //flag = false;
        if (forAll)
        {
            UnlockCharacter();
            forAll = false;
        }

        else if (!isSamePlace)
        {
            for (int i = 0; i < m_ImageLibrary.count; i++)
            {
                string tempKey = "MarkerTime" + i.ToString();
                PlayerPrefs.SetInt(tempKey, -100);
            }

            PlayerPrefs.SetInt(key, System.DateTime.Now.Hour);
            UnlockCharacter();
        }

        else if (PlayerPrefs.HasKey(key))
        {
            timestamp = PlayerPrefs.GetInt(key);

            int interval = System.DateTime.Now.Hour - timestamp;

            if (interval > intervalLimit)
            {
                PlayerPrefs.SetInt(key, System.DateTime.Now.Hour);
                UnlockCharacter();
            }
            else
            {
                if (!msgShown)
                {
                    //  StartCoroutine(ShowMsgBox("Marker could not be Reused for a short time"));
                }
            }


        }
        else
        {
            PlayerPrefs.SetInt(key, System.DateTime.Now.Hour);
            UnlockCharacter();
        }
    }

    private void UnlockCharacter()
    {
      //  m_ImageManager.trackedImagesChanged -= ImageManagerOnTrackedImagesChanged;

        int index = PlayerPrefs.GetInt("Index"); //index = entry card

        string key;

            if (index < 0)
            {

                //from bottom menu scan btn
                int totalCharaceters = PlayerPrefs.GetInt("TotalChars");

                if (SelectPetsManager.instance != null)
                {
                  
                // PlayerUnLockJson unlockJson;
                //unlockJson = SelectPetsManager.instance.GetComponent<PlayerUnLockJson>();
                for (int i = 0; i < totalCharaceters; i++)
                    {
                      key = i.ToString() + "lock";
                      //  if (!unlockJson.CheckUnlock(1, i))
                      if (PlayerPrefs.GetInt(key) == 0)
                        {
                   
                            index = i;

                            break;
                        }

                    }

                }

            }
      
              
                api.UploadCharacter((bool succeedFlag) => {
                    int language = PlayerPrefs.GetInt("Language");
                    if (succeedFlag && index >=0)
                    {
                        if (language == 0)
                        {
                            StartCoroutine(ShowMsgBox("You have successfully unlocked an “Arena of Valor” hero!"));
                            btnTxt.text = "Next";
                        }
                        else
                        {
                            StartCoroutine(ShowMsgBox("你已成功捕捉到《傳說對決》英雄 ！"));
                            btnTxt.text = "下一步";
                        }

                        Debug.Log("Unlock " + index.ToString());
                        SelectPetsManager.instance.selectedCategory = 1;
                        SelectPetsManager.instance.scanned = true;
                        SelectPetsManager.instance.selectedPetindex = index;
                        key = index.ToString() + "lock";
                        PlayerPrefs.SetInt(key, 1);

                        string date = "";
                        NotificationXML.instance.AddData(index, date);
                    }
                    else
                    {
                        processingAPI = false;
                    }
                    
                }, index);

    }

    IEnumerator ShowMsgBox(string msg)
    {
        msgShown = true;
        resultBox.SetActive(true);
        resultMsg.text = msg;
        yield return new WaitForSeconds(4);
        //resultBox.SetActive(false);
        msgShown = false;

    }

    public void LoadMenuScene()
    {
        SceneManager.LoadScene("Menu");
    }
}