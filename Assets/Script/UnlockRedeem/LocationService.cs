﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class LocationService : MonoBehaviour
{
    private Vector2[] storeLocations;


    void Start()
    {

        storeLocations = new Vector2[36];

        //old
        storeLocations[0] = new Vector2(22.248889f, 114.154883f); //香 港 仔
        storeLocations[1] = new Vector2(22.279613f, 114.182987f); //銅 鑼 灣B
        storeLocations[2] = new Vector2(22.285231f, 114.155480f);//中 環B
        storeLocations[3] = new Vector2(22.264018f, 114.237123f); //柴 灣
        storeLocations[4] = new Vector2(22.282516f, 114.221928f); //西 灣 河

        storeLocations[5] = new Vector2(22.340747f, 114.202404f); //鑽 石 山
        storeLocations[6] = new Vector2(22.305561f, 114.188625f); //紅 磡
        storeLocations[7] = new Vector2(22.312254f, 114.224643f); //觀 塘
        storeLocations[8] = new Vector2(22.318051f, 114.170166f); //旺 角A
        storeLocations[9] = new Vector2(22.316291f, 114.170481f); //旺 角B

        storeLocations[10] = new Vector2(22.322195f, 114.169299f); //旺 角C
        storeLocations[11] = new Vector2(22.331271f, 114.161934f); //深 水 埗
        storeLocations[12] = new Vector2(22.298865f, 114.172869f); //尖 沙 咀A
        storeLocations[13] = new Vector2(22.358112f, 114.127615f); //葵 涌A
        storeLocations[14] = new Vector2(22.424620f, 114.231986f); //馬 鞍 山

        storeLocations[15] = new Vector2(22.382575f, 114.187529f); //沙 田
        storeLocations[16] = new Vector2(22.502594f, 114.127893f); //上 水
        storeLocations[17] = new Vector2(22.452575f, 114.167601f); //大 埔
        storeLocations[18] = new Vector2(22.316094f, 114.265596f); //將 軍 澳
        storeLocations[19] = new Vector2(22.372736f, 114.116389f); //荃 灣A

        storeLocations[20] = new Vector2(22.370929f, 114.117945f); //荃 灣B
        storeLocations[21] = new Vector2(22.393038f, 113.976449f); //屯 門
        storeLocations[22] = new Vector2(22.443113f, 114.027873f); //元 朗A
        storeLocations[23] = new Vector2(22.444736f, 114.028295f); //元 朗B
        storeLocations[24] = new Vector2(22.445554f, 114.034576f); //元 朗C

        //new
        storeLocations[25] = new Vector2(22.279549f, 114.185903f); //銅 鑼 灣A
        storeLocations[26] = new Vector2(22.276564f, 114.174707f); //灣 仔
        storeLocations[27] = new Vector2(22.322551f, 114.212214f); //九 龍 灣
        storeLocations[28] = new Vector2(22.297827f, 114.168602f); //尖 沙 咀B
        storeLocations[29] = new Vector2(22.357201f, 114.127188f); //葵 涌B
        storeLocations[30] = new Vector2(22.307185f, 114.257373f); //將 軍 澳
        storeLocations[31] = new Vector2(22.305f, 114.17f);//Jordan
        storeLocations[32] = new Vector2(22.453f, 114.169f);//Tai Po
 
        //IFC
        storeLocations[33] = new Vector2(22.284941f, 114.156911f);

        //testing Office
        storeLocations[34] = new Vector2(22.309729f, 114.224564f); //the wave
        storeLocations[35] = new Vector2(22.314423f, 114.220106f); //smartone office

        TrackLocaton();
    }

    public void TrackLocaton()
    {
        StartCoroutine(TrackLocationRoutine());
    }

    IEnumerator TrackLocationRoutine()
    {


        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        Input.location.Start();
        //Input.location.Start(1, 0.1);

        // Wait until service initializes
        int maxWait = 20;
        Debug.Log("Initialize");
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            print("Initialize");
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            PlayerPrefs.SetInt("Place", -1);
            print("Timed out");
            yield break;
        }

        Debug.Log("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
        // PlayerPrefs.SetFloat("Latitude", Input.location.lastData.latitude);
        // PlayerPrefs.SetFloat("Longitude", Input.location.lastData.longitude);

        for (int i=0; i<storeLocations.Length; i++)
        {
            float diffX = (Input.location.lastData.latitude - storeLocations[i].x)*1000;
            float diffY = (Input.location.lastData.longitude - storeLocations[i].y)*1000;
            float distance = Mathf.Sqrt(diffX * diffX + diffY * diffY);

            //placeMsg.text = "Distance " + distance.ToString();
            PlayerPrefs.SetInt("Place", 9999);
            if (distance < 1f)
            {
                PlayerPrefs.SetInt("Place", i);
                //placeMsg.text = "Place " + i.ToString();
                break;
            }

        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            print("Unable to determine device location");
            yield break;
        }
        else
        {
            // Access granted and location value could be retrieved
          
        }

        // Stop service if there is no need to query location updates continuously


        Input.location.Stop();
    }

    public bool CheckSamePlace()
    {
        bool flag = false;
        int prevPlace = -1;

        if (PlayerPrefs.HasKey("Place"))
        {
            prevPlace = PlayerPrefs.GetInt("Place");
        }
            TrackLocaton();

            int curPlace = PlayerPrefs.GetInt("Place");

            if (prevPlace == curPlace)
                flag = true;

        

        return flag;
    }
}