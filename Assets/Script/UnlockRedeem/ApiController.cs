﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using Proyecto26;
using UnityEngine.Networking;

public class ApiController : MonoBehaviour
{


    private readonly string basePath = "https://smartar.softmind.tech/api/garena/";
    private RequestHelper currentRequest;


    [System.Serializable]
    public class RedeemClass
    {
        public int success;
        public string code;
    }

    [System.Serializable]
    public class RedeemList
    {
        public int success;
        public RedeemListItem[] list;
    }

    [System.Serializable]
    public class RedeemListItem
    {
        public string code;
        public int redeemAt;
    }

    [System.Serializable]
    public class CharacterList
    {
        public int success;
        public CharacterListItem[] list;
    }

    [System.Serializable]
    public class CharacterListItem
    {
        public int characterId;
        public long unlockedAt;
    }

    void Start()
    {
        // NotificationXML.instance.AddData("test");

        // UploadCharacter(() => { SceneManager.LoadScene("Menu"); },0) ;


        //  GetRedeemCode(() => { Debug.Log("redeem"); });
    }

    void Update()
    {

    }

    public void UploadCharacter(System.Action<bool> callback, int index)
    {
        string UserId = PlayerPrefs.GetString("userid");

        currentRequest = new RequestHelper
        {
             Uri = basePath + "unlock-user-character",
            Params = new Dictionary<string, string>
            {

            },
            BodyString = "{\"userId\":\"" + UserId + "\" , \"characterId\":" + index + "}",
            EnableDebug = true
        };

        RestClient.Post(currentRequest).Then(res => {
            if (res.Text.Contains("1"))
            {
                callback(true);
            }
            else
            {
                callback(false);
            }
            Debug.Log(res.Text);
        });
    }

    public void GetRedeemCode(System.Action callback)
    {
        string UserId = PlayerPrefs.GetString("userid");

        RedeemClass redeemClass = new RedeemClass();
        Debug.Log("redeemUserId " + UserId);
        currentRequest = new RequestHelper
        {
            Uri = basePath + "redeem",
            Params = new Dictionary<string, string>
            {

            },
            BodyString = "{\"userId\":\"" + UserId + "\"}",
            EnableDebug = true
        };

        RestClient.Post(currentRequest).Then(res => {
            Debug.Log(res.Text);
            redeemClass = JsonUtility.FromJson<RedeemClass>(res.Text);
            if (redeemClass.success == 1)
            {
                Debug.Log(redeemClass.code);
                //
                //
                string date = "";
                //
                //
                NotificationXML.instance.AddData(redeemClass.code, date);

            }
            else
            {
                //  NotificationXML.instance.AddData("fail");
            }

            callback();
        });


    }

    public void GetCharacterList(System.Action<ArrayList> callback)
    {
        // callback();
        string UserId = PlayerPrefs.GetString("userid");


        CharacterList charList = new CharacterList();

        currentRequest = new RequestHelper
        {
            Uri = basePath + "user-characters",
            Params = new Dictionary<string, string>
            {

            },
            BodyString = "{\"userId\":\"" + UserId + "\"}",
            EnableDebug = true
        };
        Debug.Log("before post " + UserId);

        RestClient.Post(currentRequest).Then(res => {
            ArrayList characterIds = new ArrayList();

            Debug.Log("CharList " + res.Text);
            charList = JsonUtility.FromJson<CharacterList>(res.Text);

            if (charList.success == 1)
            {
                // Debug.Log("charlist" + charList.list.Length);
                foreach (CharacterListItem item in charList.list)
                {
                    characterIds.Add(item.characterId);
                    // NotificationXML.instance.AddData(item.characterId);
                }
                callback(characterIds);
            }
            else
            {

                Debug.Log("loadCharerror " + res.Text);
            }

 
        });

    }

    public void GetUserRedeemList(System.Action<ArrayList> callback)
    {

        string UserId = PlayerPrefs.GetString("userid");


        RedeemList redeemList = new RedeemList();

        currentRequest = new RequestHelper
        {
            Uri = basePath + "user-redeem-list",
            Params = new Dictionary<string, string>
            {

            },
            BodyString = "{\"userId\":\"" + UserId + "\"}",
            EnableDebug = true
        };

        RestClient.Post(currentRequest).Then(res => {
            ArrayList codes = new ArrayList();


            redeemList = JsonUtility.FromJson<RedeemList>(res.Text);

            if (redeemList.success == 1)
            {

                foreach (RedeemListItem item in redeemList.list)
                {
                    codes.Add(item.code);
                    // NotificationXML.instance.AddData(item.characterId);
                }

            }
            Debug.Log("RedeemCodeRequestTxt " + res.Text);
            callback(codes);

        });
    }


}