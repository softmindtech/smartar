﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Setting : MonoBehaviour
{
    public PlayerUnLockJson m_PlayerUnlock;

    void Start()
    {
        if (InAppBrowser.IsInAppBrowserOpened())
        {
            InAppBrowser.CloseBrowser();
        }
    }


    void Update()
    {
        
    }

    public void OpenWebsite (string index)
    {
        int language = PlayerPrefs.GetInt("Language");
        if (language == 0)
        {
            language = 1;
        }
        else
        {
            language = 0;
        }
        string url = "http://www.google.com";

        if (index == "privacy")
        {
            // url =" http://waptest.smartone.com/wmc/jsp/GameZone/jsp/smartarzone_tcpp.jsp?lang="+ language.ToString()+"&type=pp";
            url = "https://wap.smartone.com/wmc/jsp/GameZone/jsp/smartarzone_tcpp.jsp?lang=" + language.ToString() + "&type=pp";
        }
        else if (index == "term")
        {
            url = "https://wap.smartone.com/wmc/jsp/GameZone/jsp/smartarzone_tcpp.jsp?lang=" + language.ToString() + "&type=tc";
        }

        InAppBrowser.DisplayOptions options = new InAppBrowser.DisplayOptions();
        //options.hidesTopBar = true;
        options.hidesHistoryButtons = true;
        //options.displayURLAsPageTitle = false;
        options.backButtonFontSize = "20";
        options.backButtonText = "X";
        options.textColor = "#000000";

        options.barBackgroundColor = "#FFFFFF";
        options.androidBackButtonCustomBehaviour = false;

        InAppBrowser.OpenURL(url,options);
    }

    public void Logout()
    {
        StartCoroutine(DelayAction());
    }

    IEnumerator DelayAction()
    {
        yield return new WaitForSeconds(0.07f);
        PlayerPrefs.DeleteAll(); // no change the best here
        SceneManager.LoadScene("Login");

    }
}
