﻿using Firebase;
using Firebase.Analytics;
using Firebase.Extensions;
using UnityEngine;
using System.Text.RegularExpressions;

public class GAtrack : MonoBehaviour
{
    
    void Start()
    {
 

        Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            var dependencyStatus = task.Result;
            if (dependencyStatus == Firebase.DependencyStatus.Available)
            {
                // Create and hold a reference to your FirebaseApp,
                // where app is a Firebase.FirebaseApp property of your application class.
                var app = Firebase.FirebaseApp.DefaultInstance;
                LogEvent("EnterMenu");
                // Set a flag here to indicate whether Firebase is ready to use by your app.
            }
            else
            {
                UnityEngine.Debug.LogError(System.String.Format(
                  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                // Firebase Unity SDK is not safe to use here.
            }
        });

    }

    public void LogEvent(string eventName)
    {
        
        //if (Application.platform == RuntimePlatform.Android)
        //{
        //    eventName = "And_" + eventName;
        //}
        //else
        //{
        //    eventName = "iOS_" + eventName;
        //}

        eventName = Regex.Replace(eventName, @" ", "");
        Firebase.Analytics.FirebaseAnalytics.LogEvent(eventName);
        Debug.Log("GA " + eventName);
    }

    public void LogCapture(string captureMode, string ARmode)
    {
        captureMode = Regex.Replace(captureMode, @" ", "");
        ARmode = Regex.Replace(ARmode, @" ", "");
        Firebase.Analytics.FirebaseAnalytics.LogEvent(captureMode, "ARmode",ARmode);
    }

    public void LogEventPets(string petType, string petColor)
    {
        petType = Regex.Replace(petType, @" ", "");
        petColor = Regex.Replace(petColor, @" ", "");
        Firebase.Analytics.FirebaseAnalytics.LogEvent(petType, "petColor", petColor);
        Debug.Log("petType " + petType + " petColor " + petColor);
    }
}
