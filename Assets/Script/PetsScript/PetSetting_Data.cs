﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PetSetting_Data : MonoBehaviour
{

    public Text PetName;
    public Text Variety;
    public Text Detail;
    public LayoutElement Detail_le;
    public Transform PreviewPoint;
    SliderSize m_sliderSize;
    ColorSelect m_ColorSelect;
    public GameObject SelectColor;
    public GameObject SetSize;

    public Text Colors_Text;
    public Text Size_Text;
    //public Image Photo;
    List<GameObject> SpwanedPets = new List<GameObject>();
    Quaternion def_rot = Quaternion.Euler(0, 210,0);
    PetBlendShapeCtrl m_PreviewPetBlendShapeCtrl;

    PetManager.PetData PreviewPetData;

    int Language;
    void Awake()
    {
        Detail_le = Detail.transform.parent.GetComponent<LayoutElement>();
        if(PlayerPrefs.HasKey("Language"))
            Language = PlayerPrefs.GetInt("Language");
        else
            Language = 1;

        m_sliderSize = GetComponentInChildren<SliderSize>();
        m_ColorSelect = GetComponentInChildren<ColorSelect>();
    }

    void Start()
    {
        

      
        m_sliderSize.asignBlendShape += ChangeBlendShapeValue;
        m_ColorSelect.asignColor += ChangeTexture;
        
        if(Colors_Text.fontSize < Size_Text.fontSize)
            Size_Text.fontSize = Colors_Text.fontSize;
        else
            Colors_Text.fontSize = Size_Text.fontSize;
    }

    public void SelectedPetData(PetManager.PetData m_selectPetData)
    {
        


        if(Language == 0)
        {
            PetName.text = m_selectPetData.name;
            Variety.text = m_selectPetData.Variety;
            Detail.text = m_selectPetData.Detail;
        }
        else
        {
            PetName.text = m_selectPetData.c_name;
            Variety.text = m_selectPetData.c_variety;
            Detail.text = m_selectPetData.c_Detail;
        }

        if(m_selectPetData.m_type == PetManager.PetType.Garena)
            Detail_le.preferredHeight = 1200;
        else
            Detail_le.preferredHeight = 500;


        GeneratePetPrefab(m_selectPetData.FourFootPrefab, m_selectPetData.m_type);
        PreviewPetData = m_selectPetData;
        //Photo.sprite = m_selectPetData.SettingPhoto;

    }
    void OnDisable()
    {
        // SelectColor.SetActive(true);
        // SetSize.SetActive(true);
    }
    void GeneratePetPrefab(GameObject spwanPet, PetManager.PetType m_type)
    {
        if(SpwanedPets.Count != 0)
        {
            bool sameObj = false;
            foreach(GameObject x in SpwanedPets)
            {
                if(x.name == string.Format("{0}(Clone)", spwanPet.name))
                {
                    sameObj = true;
                    x.transform.rotation = def_rot ;
                    x.SetActive(true);
                    m_PreviewPetBlendShapeCtrl = x.GetComponentInChildren<PetBlendShapeCtrl>();
                    bool hasBlendShape;
                    
                    if(m_type != PetManager.PetType.Garena)
                        hasBlendShape =  m_PreviewPetBlendShapeCtrl != null ? true : false;
                    else
                        hasBlendShape = false;

                    SetSize.SetActive(hasBlendShape);
                    SelectColor.SetActive(hasBlendShape);
                }
                else
                {
                    x.SetActive(false);
                }
            }
            if(sameObj == true)
                return;
            if(sameObj == false)
                SpwanPet(spwanPet, m_type);
        }
        else
        {
            SpwanPet(spwanPet, m_type);
        }
    }

    void SpwanPet(GameObject spwanPet, PetManager.PetType m_type)
    {
        GameObject pet = Instantiate(spwanPet, PreviewPoint.position, def_rot );
        m_PreviewPetBlendShapeCtrl = pet.GetComponentInChildren<PetBlendShapeCtrl>();
        bool hasBlendShape =  m_PreviewPetBlendShapeCtrl != null ? true : false;

        if(m_type != PetManager.PetType.Garena)
            hasBlendShape =  m_PreviewPetBlendShapeCtrl != null ? true : false;
        else
            hasBlendShape = false;

        SetSize.SetActive(hasBlendShape);
        SelectColor.SetActive(hasBlendShape);

        pet.transform.localScale = Vector3.one;
        pet.transform.parent = PreviewPoint;
        SpwanedPets.Add(pet);
    }

    void ChangeBlendShapeValue(float value)
    {
        if(m_PreviewPetBlendShapeCtrl != null)
        {
            m_PreviewPetBlendShapeCtrl.SetBlendShapeVale(value);
        }
    }

    void ChangeTexture(int index)
    {
        if(m_PreviewPetBlendShapeCtrl != null)
        {
            Texture m_texture = PreviewPetData.Diff_Texture[index];
            m_PreviewPetBlendShapeCtrl.ChangeMaterialTexture(m_texture);
        }
    }
}
