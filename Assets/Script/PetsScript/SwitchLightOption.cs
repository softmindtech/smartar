﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchLightOption : MonoBehaviour
{

    public Sprite[] LightSetingImgs;
    Image m_image;

    int index = 0;
    Button m_btn;
    void Start()
    {
        m_btn = GetComponent<Button>();
        m_image = GetComponent<Image>();
        if(PlayerPrefs.HasKey("LightSetting"))
        {
            index = PlayerPrefs.GetInt("LightSetting");
        }
        else
        {
            index = 0;
        }
        switchLightSetting(index);
        m_btn.onClick.AddListener(()=> switchLightSetting());
    }
    

    void switchLightSetting()
    {
        index = index == LightSetingImgs.Length -1 ? 0 : index += 1;
        m_image.sprite = LightSetingImgs[index];
        PlayerPrefs.SetInt("LightSetting",index );
    }

    void switchLightSetting(int value)
    {
        m_image.sprite = LightSetingImgs[value];
    }
}
