﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;



public class VirtualJoystick : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {
	private Image BGimage;
	private Image JoystickImage;
	private RectTransform m_rectTrans;

	private Vector3 InputVector;
	private Vector2 Original_pos;
	public bool PointerDown = false;
	void Awake()
	{
		BGimage = GetComponent<Image>();
		JoystickImage = transform.GetChild(0).GetComponent<Image>();
		m_rectTrans = transform.GetComponent<RectTransform>();
	}
	void Start()
	{

	}
	public virtual void OnPointerDown(PointerEventData ped)
	{
		PointerDown = true;
	}
	public virtual void OnPointerUp(PointerEventData ped)
	{
		InputVector = Vector3.zero;
		PointerDown = false;
		JoystickImage.rectTransform.anchoredPosition = Vector3.zero;
	}

	public void Reset()
	{
		InputVector = Vector3.zero;
		JoystickImage.rectTransform.anchoredPosition = Vector3.zero;
	}

	public virtual void OnDrag(PointerEventData ped)
	{
		Vector2 pos;
		if(RectTransformUtility.ScreenPointToLocalPointInRectangle(BGimage.rectTransform
																	,ped.position
																	,ped.pressEventCamera
																	,out pos))
		{
			pos.x = (pos.x/BGimage.rectTransform.sizeDelta.x);
			pos.y = (pos.y/BGimage.rectTransform.sizeDelta.y);
			InputVector = new Vector3(pos.x * 2 + 1, 0 ,pos.y * 2 -1);
			InputVector = (InputVector.magnitude > 1.0f)? InputVector.normalized:InputVector;

			JoystickImage.rectTransform.anchoredPosition = new Vector3(InputVector.x *(BGimage.rectTransform.sizeDelta.x /4f)
																		,InputVector.z * (BGimage.rectTransform.sizeDelta.y/4f)
																		,0);
		}
	}

	public float Horizontal()
	{
		if(InputVector.x != 0)
			return InputVector.x;
		else
			return Input.GetAxis("Horizontal");
	}

	public float Vertical()
	{
		if(InputVector.x != 0)
			return InputVector.z;
		else
			return Input.GetAxis("Vertical");
	}

	public Quaternion Direction()
	{
		Vector3 dir = Vector3.zero - new Vector3(Horizontal(), 0, Vertical());
		Quaternion rot = Quaternion.LookRotation(dir);
		Vector3 offset = new Vector3(0, -90, 0);
		Quaternion offset_rot = Quaternion.Euler(offset);
		rot *= offset_rot;
		return rot;
	}
}
