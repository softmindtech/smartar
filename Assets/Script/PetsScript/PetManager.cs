﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;



[CreateAssetMenu(menuName = "CreateData/PetData")]
public class PetManager : ScriptableObject
{
    public enum PetType{
        Cat = 0,
        Garena = 1,
        Dog = 2
    }

    public enum PetColorName{

        White,
        White_Yellow,
        White_Grey,
        White_Brown,
        Black,
        LightYellow,
        Yellow,
        Brown,
        White_Grey_Brown,
        White_Yellow_Grey,
        White_Yellow_Black,
        White_Brown_Black,
        White_Yellow_Brown

    }
    [System.Serializable]
    public class PetData
    {
        public string name;
        public string c_name;
        public bool Lock = false;
        public string Variety;
        public string c_variety;
        
        [TextArea(5,10)]
        public string Detail;
        
        [TextArea(5,10)]
        public string c_Detail;
        public PetType m_type;
        public Sprite MenuPhoto;
        public Sprite SettingPhoto;
        public GameObject FourFootPrefab;
        public Texture[] Diff_Texture;
        public Sprite[] Diff_Color;
        public PetColorName[] m_petColorName;
    
    }
    
    public bool spwaned = false;
    public PetData m_petDetail;
}
