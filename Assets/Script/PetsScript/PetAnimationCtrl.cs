﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class PetAnimationCtrl : MonoBehaviour
{
    public Action endAction;

    // Start is called before the first frame update
    public void ActionEnd()
    {
        if(endAction != null)
        {
            endAction();
            endAction = null;
        }

    }
}
