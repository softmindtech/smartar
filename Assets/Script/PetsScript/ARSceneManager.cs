﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class ARSceneManager : MonoBehaviour
{

    public PetsCategory[] m_petsCategory;

    public PetBlendShapeCtrl[] m_PetBlendShapeCtrl;
    float BlendShapeValue;
    public ARPlaceOnPlane m_ARplaceOnPlane;
    //public HumanoidTracker  m_HumanoidTracker;
    Action<bool> SwtichModel;


    int selectedCategory = 0;
    int selectedIndex = 0;
    float selectedPetSize = 0;

    void Awake()
    {
        if(!PlayerPrefs.HasKey("Category") ||
            !PlayerPrefs.HasKey("PetIndex") ||
            !PlayerPrefs.HasKey("PetBlendShape") )
        {
            Debug.Log("There are not PlayerPrefs");
            return;
        }
        selectedCategory = PlayerPrefs.GetInt("Category");
        selectedIndex = PlayerPrefs.GetInt("PetIndex"); 
        selectedPetSize = PlayerPrefs.GetFloat("PetBlendShape");
    }
    // Start is called before the first frame update

    GameObject AnimalNormalModel;
    GameObject AnimalHumanModel;
    void Start()
    {
        PetManager selectedPetManager =  m_petsCategory[selectedCategory].m_Petsmanager[selectedIndex];
        AnimalNormalModel = Instantiate(selectedPetManager.m_petDetail.FourFootPrefab, 
                                                    transform.position, 
                                                    Quaternion.identity);
       
        m_PetBlendShapeCtrl[0] = AnimalHumanModel.GetComponentInChildren<PetBlendShapeCtrl>();
        m_PetBlendShapeCtrl[1] = AnimalNormalModel.GetComponentInChildren<PetBlendShapeCtrl>();

        StartCoroutine(DelayStartAction());
        
        
        
    }

    IEnumerator DelayStartAction()
    {
        yield return new WaitForSeconds(0.1f);
        
        for (int i = 0; i < m_PetBlendShapeCtrl.Length; i++)
        {
            m_PetBlendShapeCtrl[i].SetBlendShapeVale(selectedPetSize);
        }

        //SwtichModel += m_ARplaceOnPlane.Swicthstate;
        //SwtichModel += m_HumanoidTracker.Swicthstate;
        
        SwtichModel(false);
    }

    public void  SwtichModelState(bool value)
    {
        if(SwtichModel != null)
            SwtichModel(value);
    }

}
