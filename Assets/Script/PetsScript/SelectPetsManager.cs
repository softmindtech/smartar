﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif
public class ConfirmedPetsData
{
    public int m_category;
    public int Petindex;
    public float Petsize;

    public ConfirmedPetsData(int set_categort, int index, float sizeValue)
    {
        m_category = set_categort;
        Petindex = index;
        sizeValue = Petsize;
    }
}

[System.Serializable]
public class PetsCategory
{
    public string name;
    public PetManager[] m_Petsmanager;
}

public enum petCategory
{
    cat = 0,
    dog = 1,
    lastindex = 2,
}
public class SelectPetsManager : MonoBehaviour
{
    
    public static SelectPetsManager instance;

    public PetsCategory[] m_petsCategory;

    public int selectedCategory;
    public int selectedPetindex;
    public int ColorSelect;
    public float sizeValue;
    
    public bool scanned = false;
    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
            Destroy(this);
    }

    void Start()
    {
        #if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
        {
            Permission.RequestUserPermission(Permission.Camera);
            Permission.RequestUserPermission(Permission.Microphone);
        }
        #endif
    }
    public PetManager.PetData GetSelectPetData()
    {
       return m_petsCategory[selectedCategory].m_Petsmanager[selectedPetindex].m_petDetail;
    }


    
   
    


}
