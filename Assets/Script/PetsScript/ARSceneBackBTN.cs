﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NatSuite.Examples;

public class ARSceneBackBTN : MonoBehaviour
{

    
    public MiniCam m_miniCam;
    public GameObject BlockImgae;
    LoadScene m_loadScene;

    void Start()
    {
        m_loadScene = GetComponent<LoadScene>();
        m_loadScene.m_extraFunc += ARBackExtraFunc;
    }

    void ARBackExtraFunc()
    {
        BlockImgae.SetActive(true);
        m_miniCam.StopCamera();
    }
}
