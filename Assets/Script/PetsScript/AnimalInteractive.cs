﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalInteractive : MonoBehaviour
{
    Camera m_camera;

    // Start is called before the first frame update
    void Start()
    {
        m_camera = GetComponent<Camera>();

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.touchCount == 0)
            return;
        
        Touch t = Input.GetTouch(0);
        Ray ray = m_camera.ScreenPointToRay(t.position);
        RaycastHit hit;
        if(t.phase == TouchPhase.Began)
        {
            if(Physics.Raycast(ray, out hit, 1 << 10))
            {
                Animator animal = hit.collider.GetComponent<Animator>();
                if(animal != null)
                {
                    if(animal.GetCurrentAnimatorStateInfo(0).IsTag("default"))
                    {
                        animal.SetTrigger("Jump");
                    }
                }
            }
        }
    }
}
