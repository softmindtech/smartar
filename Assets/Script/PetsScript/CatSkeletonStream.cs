﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatSkeletonStream : MonoBehaviour
{
    public Transform[] CatSkeleton;

    public Transform[] HumanSkeleton;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(HumanSkeleton.Length != 0)
        {
            UpdateCatSkeleton();
        }
    }

    void UpdateCatSkeleton()
    {
        for (int i = 0; i < CatSkeleton.Length; i++)
        {
            if(CatSkeleton[i] != null)
            {
                CatSkeleton[i].localPosition = HumanSkeleton[i].localPosition;
                CatSkeleton[i].localRotation = HumanSkeleton[i].localRotation;

            }
        }
    }
}
