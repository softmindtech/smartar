﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using FrostweepGames.Plugins.GoogleCloud.SpeechRecognition;
using System;

public class micophoneCtrl : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    Button m_button;
    private GCSpeechRecognition _speechRecognition;
    // Start is called before the first frame update
    bool startRecord = false;
    Text OrderTest;
    public string[] _commands;
    public CanvasGroup m_OrderCanvasGroup;
    public Action<int> orderAction;
    [SerializeField]
    bool processEnd = true;
    void Start()
    {
        m_button = GetComponent<Button>();
        _speechRecognition = GCSpeechRecognition.Instance;
        _speechRecognition.RecognizeSuccessEvent += RecognizeSuccessEventHandler;
        _speechRecognition.RecognizeFailedEvent += RecognizeFailedEventHandler;

        _speechRecognition.FinishedRecordEvent += FinishedRecordEventHandler;
        _speechRecognition.StartedRecordEvent += StartedRecordEventHandler;
        _speechRecognition.RecordFailedEvent += RecordFailedEventHandler;

        _speechRecognition.EndTalkigEvent += EndTalkigEventHandler;

        _speechRecognition.RequestMicrophonePermission(null);
        if (_speechRecognition.HasConnectedMicrophoneDevices())
        {
            _speechRecognition.SetMicrophoneDevice(_speechRecognition.GetMicrophoneDevices()[0]);
        }

        OrderTest = m_OrderCanvasGroup.GetComponentInChildren<Text>();
        m_OrderCanvasGroup.alpha = 0;
    }
    float pressTime = 0;
    void Update()
    {
        if(startRecord)
            pressTime += Time.deltaTime;
        else
            pressTime = 0;
    }
    private void OnDestroy()
    {
        _speechRecognition.RecognizeSuccessEvent -= RecognizeSuccessEventHandler;
        _speechRecognition.RecognizeFailedEvent -= RecognizeFailedEventHandler;

        _speechRecognition.FinishedRecordEvent -= FinishedRecordEventHandler;
        _speechRecognition.StartedRecordEvent -= StartedRecordEventHandler;
        _speechRecognition.RecordFailedEvent -= RecordFailedEventHandler;

        _speechRecognition.EndTalkigEvent -= EndTalkigEventHandler;
    }
    public void OnPointerDown(PointerEventData eventData)
    {   
        if(m_button.interactable && processEnd)
        {
            processEnd = false;
            startRecord = true;
            _speechRecognition.StartRecord(false);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        if(m_button.interactable && startRecord)
        {
            startRecord = false;
            _speechRecognition.StopRecord();
        }
    }
    private void StartedRecordEventHandler()
    {
        StartCoroutine(FadeOrderCanvasGroup(1));
        OrderTest.text = "start record";
    }
    private void RecordFailedEventHandler()
    {
        
        OrderTest.text = "Order Fail";

    }
    private void FinishedRecordEventHandler(AudioClip clip, float[] raw)
    {

        OrderTest.text = "End record";
        if (clip == null)
            return;

        RecognitionConfig config = RecognitionConfig.GetDefault();
        config.languageCode = ((Enumerators.LanguageCode)15).Parse();
        config.audioChannelCount = clip.channels;
        // configure other parameters of the config if need

        GeneralRecognitionRequest recognitionRequest = new GeneralRecognitionRequest()
        {
            audio = new RecognitionAudioContent()
            {
                content = raw.ToBase64()
            },
            //audio = new RecognitionAudioUri() // for Google Cloud Storage object
            //{
            //	uri = "gs://bucketName/object_name"
            //},
            config = config
        };

        _speechRecognition.Recognize(recognitionRequest);
    }
    private void EndTalkigEventHandler(AudioClip clip, float[] raw)
    {
        FinishedRecordEventHandler(clip, raw);
    }

    void RecognizeSuccessEventHandler(RecognitionResponse recognitionResponse)
    {
        OrderTest.text = "Order: ";

        string[] commands = _commands;

        foreach (var result in recognitionResponse.results)
        {
            foreach (var alternative in result.alternatives)
            {
               //OrderTest.text += alternative.transcript.ToLowerInvariant() + "\n";
                foreach (var command in commands)
                {
                    if (command.ToLowerInvariant() == alternative.transcript.ToLowerInvariant())
                    {
                        
                        DoCommand(command.ToLowerInvariant());
                        return;
                    }
                }
            }
            
        }
        DoCommand("fail");
    }

    private void DoCommand(string command)
    {
        Debug.Log(command);
        switch (command)
        {
            case "come":
            case "comb":
            case "come on":
                if(orderAction != null)
                    orderAction(0);
                OrderTest.text += "Come";
            break;
            case "meow":
            case "mail":
            case "mayo":
            case "neil":
                if(orderAction != null)
                    orderAction(1);
                OrderTest.text += "Meow";
            break;
            case "sit":
            case "sit down":
            case "cit":
            case "sid":
                if(orderAction != null)
                    orderAction(2);
                OrderTest.text += "Sit";
            break;
            default :
                OrderTest.text += "Fail";
            break;
        }
        
        StartCoroutine(FadeOrderCanvasGroup(0));
    }

    IEnumerator FadeOrderCanvasGroup(float fade)
    {
        if(fade <= 0)
            yield return new WaitForSeconds(1);
        
        float elaspe = 0;
        float elaspeTime = 0.5f;
        while(elaspe < elaspeTime)
        {
            m_OrderCanvasGroup.alpha = Mathf.Lerp(m_OrderCanvasGroup.alpha, fade, elaspe/elaspeTime);
            elaspe += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }

        if(fade == 0)
            processEnd = true;
    }
    public void CompulsoryEnd()
    {
        StopAllCoroutines();
        startRecord = false;
        _speechRecognition.StopRecord();
        m_OrderCanvasGroup.alpha = 0;
    }

    
    private void RecognizeFailedEventHandler(string error)
    {
        OrderTest.text = "Recognize Failed";
    }
    // Update is called once per frame
}
