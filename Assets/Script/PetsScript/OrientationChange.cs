﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using NatSuite.Examples;


public class OrientationChange : MonoBehaviour
{
    //public Text m_testText;
    ARPlaneManager m_planeManager;
    [HideInInspector]
    public GameObject model;
    public CanvasScaler menu_canvasScaler;
    public Button BackBtn;

    public RectTransform m_PreviewImage;
    public MiniCam m_miniCam;
    public switchCamera m_switchCam;


    public enum Orientationstate
    {
        Portrait,
        Landscape
    }
    Orientationstate m_state;
    // Start is called before the first frame update
    void Start()
    {
        m_planeManager = GetComponent<ARPlaneManager>();
        m_state = Orientationstate.Portrait;
        BackBtn.onClick.AddListener(ResetOrientation);
    }
    void ResetOrientation()
    {
        Screen.orientation = ScreenOrientation.Portrait;
    }
    // Update is called once per frame
    void Update()
    {
        if (Input.deviceOrientation==DeviceOrientation.Portrait || 
            Input.deviceOrientation==DeviceOrientation.PortraitUpsideDown) 
        {   
            if(m_state == Orientationstate.Landscape)
            {
                m_state = Orientationstate.Portrait;
                Quaternion rot = Quaternion.Euler(0,180,0);
                model.transform.localRotation =  rot;
                
                rot = Quaternion.Euler(0,0,0);
                // m_PreviewImage.transform.localRotation = rot;
                // m_PreviewImage.transform.localScale = new Vector3(1.2f,1.2f, 1);
                Screen.orientation = ScreenOrientation.Portrait;
                menu_canvasScaler.referenceResolution = new Vector2(800,600);
                menu_canvasScaler.matchWidthOrHeight = 0;
                if(m_switchCam.cameraSwicth)
                    m_miniCam.AutoRotateChange();
            }
        }
        else if(Input.deviceOrientation==DeviceOrientation.LandscapeLeft || 
                Input.deviceOrientation==DeviceOrientation.LandscapeRight)
        {
            if(m_state == Orientationstate.Portrait)
            {
                if(Input.deviceOrientation == DeviceOrientation.LandscapeLeft)
                {
                    Quaternion rot = Quaternion.Euler(0,0,90);
                   // model.transform.localRotation = model.transform.localRotation * rot;
                    // m_PreviewImage.transform.localRotation = rot;
                    
                    // m_PreviewImage.transform.localScale = Vector3.one;
                    Screen.orientation = ScreenOrientation.LandscapeLeft;
                }
                else if(Input.deviceOrientation == DeviceOrientation.LandscapeRight)
                {
                    Quaternion rot = Quaternion.Euler(0,0,-90);
                    //model.transform.localRotation = model.transform.localRotation * rot;
                    
                    // m_PreviewImage.transform.localRotation = rot;
                    // m_PreviewImage.transform.localScale = Vector3.one;
                    Screen.orientation = ScreenOrientation.LandscapeRight;
                }
                menu_canvasScaler.referenceResolution = new Vector2(800,1000);
                menu_canvasScaler.matchWidthOrHeight = 1;
                m_state = Orientationstate.Landscape;

                if (m_switchCam.cameraSwicth)
                    m_miniCam.AutoRotateChange();
            }
            
        }
    }
}
