﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PetBlendShapeCtrl : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    SkinnedMeshRenderer skinnedMeshRenderer;
    public int[] BlendShapeindex;
    public int MaterialIndex;


    float BlendShapeValue;

    void OnEnable()
    {
        // if(SceneManager.GetActiveScene().name == "SampleScene")
        // {
        //     float furLength = skinnedMeshRenderer.materials[MaterialIndex].GetFloat("_FurLength");
        //     skinnedMeshRenderer.materials[MaterialIndex].SetFloat("_FurLength", furLength/10f);
        // }

    }
    void Awake()
    {
        skinnedMeshRenderer = GetComponent<SkinnedMeshRenderer>();
    }
    public void ChangeMaterialTexture(Texture m_texture)
    {
        skinnedMeshRenderer.materials[MaterialIndex].SetTexture("_MainTex", m_texture);
    }
    public void SetBlendShapeVale(float value)
    {
        if(BlendShapeindex.Length != 0)
        {

            if(value > 0 ) //fat
            {
                BlendShapeValue = value;
                skinnedMeshRenderer.SetBlendShapeWeight(BlendShapeindex[1], BlendShapeValue );
                skinnedMeshRenderer.SetBlendShapeWeight(BlendShapeindex[0], 0 );
            }
            else if( value < 0) // thin
            {
                BlendShapeValue = -value;
                skinnedMeshRenderer.SetBlendShapeWeight(BlendShapeindex[0], BlendShapeValue );
                skinnedMeshRenderer.SetBlendShapeWeight(BlendShapeindex[1], 0 );
            }
            else
            {
                skinnedMeshRenderer.SetBlendShapeWeight(BlendShapeindex[1], 0 );
                skinnedMeshRenderer.SetBlendShapeWeight(BlendShapeindex[0], 0 );
            }
        }
    }

    public float GetBlendShapeValue()
    {
        return  skinnedMeshRenderer.GetBlendShapeWeight(BlendShapeindex[1]) - 
                skinnedMeshRenderer.GetBlendShapeWeight(BlendShapeindex[0]);
    }
}
