﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Threading.Tasks;
using NatSuite.Examples;



public class LoadScene : MonoBehaviour
{
    Button m_button;
    public string SceneName;
    public delegate void ExtraFunc();

    public ExtraFunc m_extraFunc;

    // Start is called before the first frame update
    void Start()
    {
        m_button = GetComponent<Button>();
        m_button.onClick.AddListener(() => GoToOtherScene());
    }

    void GoToOtherScene()
    {
        PlayerPrefs.SetInt("Index", -100);
        if(m_extraFunc != null)
            m_extraFunc();
        DelayWhenLandscape();
    }

    async void DelayWhenLandscape()
    {


        if(Input.deviceOrientation==DeviceOrientation.LandscapeLeft || 
                Input.deviceOrientation==DeviceOrientation.LandscapeRight)
        {
            await Task.Delay(500);
            SceneManager.LoadScene(SceneName);
        }
        else
        {
            SceneManager.LoadScene(SceneName);
        }
    }

}
