﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomBtnObj : MonoBehaviour
{
    public Image m_image;
    public Sprite originSprite;
    // Start is called before the first frame update
    void Awake()
    {
        m_image = GetComponent<Image>();
        originSprite = m_image.sprite;

    }

    public void  ChageOrigin(Sprite c_sprite)
    {
        originSprite = c_sprite;
    }
    public void Deselect()
    {
        m_image.sprite = originSprite;
    }
    // Update is called once per frame
}
