﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.EventSystems;

public class ScrollRectSnap_Menu : MonoBehaviour, IPointerUpHandler, IPointerDownHandler, IDragHandler
{

    public RectTransform MenuList;
    public RectTransform Content;
    HorizontalLayoutGroup Content_HLG; 
    ScrollRect m_scrollRect;
    float m_rectOffset;
    
    public int PetListCount;
    public float PetsWidth{get; private set;}
    Vector2 SnapPos;
    public bool setUpfinish = false;
    
    void Awake()
    {
        ListIndex = 0;
    }
    void Start()
    {
        m_scrollRect = GetComponent<ScrollRect>();
        Content_HLG = Content.GetComponentInChildren<HorizontalLayoutGroup>();
        RectOffset content_padding = Content_HLG.padding;
        content_padding.left = (int)(Screen.width / 1080f * 120f);
        Content_HLG.padding = content_padding;
    }
    
    MenuListCtrl ActiveMenuList;
    public void ChangePetsList(RectTransform selectPetsList, int ListCount)
    {
        if(MenuList != null)
            MenuList.gameObject.SetActive(false);
        ActiveMenuList = selectPetsList.GetComponent<MenuListCtrl>();
        PetListCount = ListCount;
        Content.anchoredPosition = new Vector2(0,  Content.anchoredPosition.y);
        MenuList = selectPetsList;
        MenuList.gameObject.SetActive(true);
        StartCoroutine(DelayAction());
    }

    IEnumerator DelayAction()
    {
        yield return new WaitForSeconds(0.1f);
        PetsWidth = MenuList.rect.width / PetListCount;
        m_rectOffset = PetsWidth/2 ;
        setUpfinish = true;
        StartPosition(0);
        
    }
    [SerializeField]
    bool drug = false;
    bool pointerUp = false;
    float offset = 30;
    public void OnPointerDown(PointerEventData eventData)
    {
        click = true;
        drug = false;
        pointerUp = false;
    }
    public int ListIndex{get; private set;}
    void Update()
    {
        if(m_scrollRect.velocity.magnitude < 1300 && drug && pointerUp)
        {
            calculateSnapPos();
            drug = false;
        }
        ContentposX = -Content.anchoredPosition.x ;
        ContentposX += m_rectOffset;
        int x = (int)(ContentposX/PetsWidth);
        ListIndex = x >= PetListCount ? PetListCount -1 : x;
    }
    public void OnDrag(PointerEventData eventData)
    {
        click = false;
        drug = true;
    }
    public float ContentposX{get; private set;}
    public void ClickAction()
    {
        ActiveMenuList.ClickButtonAction(ListIndex);
    }
    public bool click;
    public void OnPointerUp(PointerEventData eventData)
    {
        if(click)
        {
            ClickAction();
            click = false;
        }
        pointerUp = true;
    }
    void calculateSnapPos()
    {

        if(ContentposX >  MenuList.rect.width)
        {
            SnapPos = new Vector2 (-(PetsWidth * (PetListCount -1)) , Content.anchoredPosition.y);
            StartCoroutine(Snap());
            return;
        }
        
        for (int i = 0; i < PetListCount; i++)
        {
            if(ContentposX > PetsWidth * i && ContentposX < PetsWidth * (i +1))
            {
                
                SnapPos = new Vector2 (-(PetsWidth * i), Content.anchoredPosition.y);
                StartCoroutine(Snap());
            }
        }
    }
    public void StartPosition(int index)
    {
        SnapPos = new Vector2 (-(PetsWidth * index) , Content.anchoredPosition.y);
        StartCoroutine(Snap());
    }
    IEnumerator Snap()
    {
        m_scrollRect.enabled = false;
        float elaspeTime = 0;
        float waitTime = 2f;
        while(Vector2.Distance(Content.anchoredPosition, SnapPos) > 0.1f + Mathf.Epsilon)
        {
            elaspeTime += Time.deltaTime;
            Content.anchoredPosition = Vector2.Lerp(Content.anchoredPosition, SnapPos, elaspeTime / waitTime);
            yield return new WaitForEndOfFrame();
        }
        
        m_scrollRect.enabled = true;
    }
}
