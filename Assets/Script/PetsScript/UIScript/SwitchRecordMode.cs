﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NatSuite.Examples.Components;
using UnityEngine.UI;

public class SwitchRecordMode : MonoBehaviour
{
    public RecordButton m_recordBtn;
    public RecordManager m_recordmanager;
    Button m_btn;
    public bool swicthVideo = false;
    Text[] switchBtnsText;
    public Button[] switchBtns;

    public delegate void uploadData();
    public uploadData m_upload;

    // Start is called before the first frame update
    void Start()
    {
        switchBtnsText = new Text[switchBtns.Length];
        m_btn = GetComponent<Button>();

        switchBtns[0].onClick.AddListener(()=>SwitchMode(false, 0));
        switchBtnsText[0] = switchBtns[0].GetComponentInChildren<Text>();

        switchBtns[1].onClick.AddListener(()=>SwitchMode(true, 1));
        switchBtnsText[1] = switchBtns[1].GetComponentInChildren<Text>();

        switchBtns[0].onClick.Invoke();
    }

    public void SwitchMode(bool value, int x)
    {
        swicthVideo = value;
        m_recordBtn.enabled = swicthVideo;
        for (int i = 0; i < switchBtnsText.Length; i++)
        {
            switchBtnsText[i].color = i == x ? new Color(1,0.2f,0) : new Color(1,1,1);
        }
        if(swicthVideo)
        {
            m_btn.onClick.RemoveAllListeners();
        }
        else
        {
            m_btn.onClick.AddListener(m_recordmanager.CapturePhoto);
            m_btn.onClick.AddListener(UploadData);
        }

    }

    public void UploadData()
    {
        if(m_upload != null)
            m_upload();
    }

}
