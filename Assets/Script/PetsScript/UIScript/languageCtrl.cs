﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Xml.Linq;


public class languageCtrl : MonoBehaviour
{
    public Text[] m_text;

    public TextAsset xmlRawFile;
    int Language;
    // 1 == Chi
    // 0 == Eng
    
    string xmlLang;
    void Start()
    {
        string data = xmlRawFile.text;
        
        if(PlayerPrefs.HasKey("Language"))
            Language = PlayerPrefs.GetInt("Language");
        else
            Language = 1;

        xmlLang = Language == 0 ? "English" : "Chinese";

        RealXml(data);
    }


    void RealXml(string xmldata)
    {
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(new StringReader (xmldata));

        string xmlPathPattern = "//languages/" + xmlLang;
        XmlNodeList myNodeList = xmlDoc.SelectNodes(xmlPathPattern);
        foreach(XmlNode node in myNodeList)
        {
            XmlNode fNode = node.FirstChild;
            XmlNode nNode = fNode;

            while(nNode != node.LastChild)
            {
                for (int i = 0; i < m_text.Length; i++)
                {
                    if(m_text[i].name == nNode.Name)
                    {
                        m_text[i].text = nNode.InnerText;
                        break;
                    }
                }
                nNode = nNode.NextSibling;
            }
            
        }

    }
}
