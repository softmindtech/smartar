﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class PageCtrl : MonoBehaviour
{
    public static PageCtrl instace;
    public GameObject BlockUIImage;
    public GameObject[] Pages;
    void Awake()
    {
        if(instace == null)
            instace = this;
        BlockUIImage.GetComponent<Image>().color = new Color(0,0,0,1);
    }
    public void SwtichPage(int index)
    {
        for (int i = 0; i < Pages.Length; i++)
        {
            Pages[i].SetActive( (i == index || i == 1)? true : false);
        }
    }
    void Start()
    {
        
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(1);
        BlockUIImage.SetActive(false);
    }
}
