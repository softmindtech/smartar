﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AspectRatioCtrl : MonoBehaviour
{
   
    Image m_image;
    RectTransform m_rectTrans;
    
	[SerializeField]
	float screenHeighRef = 1920;
	[SerializeField]
	float screenWidthRef = 1080;
	float screenRaitoRef;
    // Start is called before the first frame update
    void Start()
    {
        m_rectTrans = GetComponent<RectTransform>();

        screenRaitoRef = screenWidthRef/screenHeighRef;
        float DeviceScreenRatio = (float)Screen.width/(float)Screen.height;

		if(screenRaitoRef < DeviceScreenRatio)  //IOS
		{
            float ImageHeigh = screenHeighRef * ((float)Screen.width/screenWidthRef);
			m_rectTrans.sizeDelta = new Vector2((float)Screen.width, ImageHeigh);

		}		
		else if(screenRaitoRef > DeviceScreenRatio) // wide
		{
            float Imagewidth = screenWidthRef * ((float)Screen.height/screenHeighRef);
			m_rectTrans.sizeDelta = new Vector2(Imagewidth, (float)Screen.height);
		}
        else
        {
            m_rectTrans.sizeDelta = new Vector2((float)Screen.width, (float)Screen.height);
        }


    }

}
