﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ColorSelect : MonoBehaviour
{

    public GameObject ColorBtnPrefab;
    public List<GameObject> ColorBtns;
    
    public GameObject selector;

    SelectPetsManager m_manager;

    void Start()
    {
        if(SelectPetsManager.instance != null)
            m_manager = SelectPetsManager.instance;
        
    }

    public void GenerateColor(int ColorBtnCount, Sprite[] BtnColors)
    {
        if(ColorBtns.Count != 0)
        {
            GameObject[] m_ColorBtnsArray = new GameObject[ColorBtns.Count];
            m_ColorBtnsArray = ColorBtns.ToArray();
            if(ColorBtns.Count >= ColorBtnCount)
            {
                for (int i = 0; i < ColorBtns.Count; i++)
                {
                    if(i < ColorBtnCount)
                    {
                        m_ColorBtnsArray[i].GetComponent<Image>().sprite = BtnColors[i];
                        m_ColorBtnsArray[i].SetActive(true);
                    }
                    else
                    {
                        m_ColorBtnsArray[i].SetActive(false);
                    }


                    if(i == 0)
                        StartCoroutine(DelayAction(m_ColorBtnsArray[i].transform.position));
                }
            }
            else
            {
                for (int i = 0; i < ColorBtnCount; i++)
                {
                    if(i < ColorBtns.Count)
                    {
                        m_ColorBtnsArray[i].GetComponent<Image>().sprite = BtnColors[i];
                        m_ColorBtnsArray[i].SetActive(true);
                    }
                    else
                    {
                        spwanPrefab(i,BtnColors[i]);
                    }
                    
                    if(i == 0)
                        StartCoroutine(DelayAction(m_ColorBtnsArray[i].transform.position));
                }
            }
        }
        else
        {
            for (int i = 0; i < ColorBtnCount; i++)
            {
               spwanPrefab(i,BtnColors[i]);
            }
        }
        
    }
    void spwanPrefab(int index, Sprite m_color)
    {
        GameObject ColorBtn = Instantiate(ColorBtnPrefab, transform.position, Quaternion.identity);
        ColorBtn.transform.parent = this.transform;
        ColorBtn.transform.localScale = Vector3.one;
        ColorBtns.Add(ColorBtn);
        int x = index;
        ColorBtn.GetComponent<Image>().sprite = m_color;
        ColorBtn.GetComponent<Button>().onClick.AddListener(() => SelectColor(x, ColorBtn.transform.position));
        if(index == 0 )
            StartCoroutine(DelayAction(ColorBtn.transform.position));
    }

    IEnumerator DelayAction(Vector3 pos)
    {
        yield return new WaitForSeconds(0.05f);
        //SelectColor(0, pos);
        foreach(GameObject x in ColorBtns)
        {
            x.GetComponent<Button>().onClick.Invoke();
            yield break;
        }
    }

    public Action<int> asignColor;
    void SelectColor(int index, Vector3 pos)
    {
        m_manager.ColorSelect = index;
        selector.transform.position =  pos;
        if(asignColor != null)
            asignColor(index);

    }   
}
