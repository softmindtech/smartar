﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


[RequireComponent(typeof(Button))]
public class ButtonClass : MonoBehaviour
{

    public Button m_btn;
    public Image m_image;
    public PageCtrl m_pageCtrl;

    public MenuController m_menuCtrl;
    // Start is called before the first frame update

    public void SetUp()
    {
        if(PageCtrl.instace != null)
            m_pageCtrl = PageCtrl.instace;
        if(MenuController.instacene != null)
            m_menuCtrl = MenuController.instacene;
        m_btn = GetComponent<Button>();
        m_image =GetComponent<Image>();
        m_btn.onClick.AddListener(delegate {ButtonClick();});
    }
    public virtual void ButtonClick()
    {

    }
}
