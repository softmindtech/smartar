﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ChangeLanguageBTN : MonoBehaviour
{

    int Language;
    public Button CH_button;
    public Button Eng_button;
    
    Image CH_Image;
    Image Eng_image;
    Text CHBTNText;
    Text EngBTNText;

    Button m_button;
    public GameObject Block_popUpMSG;

    // Start is called before the first frame update

    void Start()
    {
        if(PlayerPrefs.HasKey("Language"))
            Language = PlayerPrefs.GetInt("Language");
        else
            Language = 1;

        m_button = GetComponent<Button>();
        m_button.onClick.AddListener(ChangeLangMsgPopUP);
        CHBTNText = CH_button.GetComponentInChildren<Text>();
        EngBTNText = Eng_button.GetComponentInChildren<Text>();

        CH_Image = CH_button.GetComponent<Image>();
        Eng_image = Eng_button.GetComponent<Image>();

        if(Language == 0)
        {
            CH_Image.color = new Color(1,1,1,0);
            Eng_image.color = new Color(1,1,1,1);
            CHBTNText.color = Color.black;
            EngBTNText.color = Color.white;
            CH_button.onClick.AddListener(() => RestartApp(1));
        }
        else
        {
            CH_Image.color = new Color(1,1,1,1);
            Eng_image.color = new Color(1,1,1,0);
            CHBTNText.color = Color.white;
            EngBTNText.color = Color.black;
            Eng_button.onClick.AddListener(() => RestartApp(0));
        }
        
        
    }


    void ChangeLangMsgPopUP()
    {
        Block_popUpMSG.SetActive(true);
    }

    void ClosePopUpMsg()
    {
        Block_popUpMSG.SetActive(false);
    }

    void RestartApp(int index)
    {
        
        PlayerPrefs.SetInt("Language", index);
        Scene acrtivescene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(acrtivescene.name);
    }

    
}
