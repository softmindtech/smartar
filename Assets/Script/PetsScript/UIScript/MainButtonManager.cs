﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MainButtonManager : ButtonClass
{

    //   Class  Button m_btn;
    //   Class  MenuController m_menuCtrl;
    public enum BtnType
    {
        ComfirmBtn = 0,
        SelectPet = 1,
        MenuSelect = 2,
        LastIndex = 3
    }

    public BtnType m_type;
    public Action ButtonAction;
    SelectPetsManager m_manager;
    PlayerUnLockJson m_playerUnlock;
    void Start()
    {
        if(SelectPetsManager.instance != null)
        {
            m_manager = SelectPetsManager.instance;
            m_playerUnlock = m_manager.GetComponent<PlayerUnLockJson>();
        }

        SetUp();

        switch(m_type)
        {
            case BtnType.ComfirmBtn:
                ButtonAction += ComfirmBtnAction;
            break;
            case BtnType.SelectPet:
                ButtonAction += SelectorComfirm;
                Scanned();
            break;
            case BtnType.MenuSelect:
                ButtonAction += MenuSelectAction;
            break;
            
            default:
            break;
        }

        

    }

    public override void ButtonClick()
    {
        if(ButtonAction != null)
            ButtonAction();
    }


    void ComfirmBtnAction()
    {

        SceneManager.LoadScene(1);
    }
    

    public ColorSelect m_ColorSelect;
    public Selector m_CategorySelector;
    public PetSetting_Data m_PetSetting_Data;
    public GameObject BottomMenu;
    public GameObject BottomMenuBG;
    PetManager.PetData m_SelectPetData;
    public void SelectorComfirm()
    {
        m_CategorySelector.confirmSelectPet();
        SelectPetAction();
    }
    public void SelectPetAction()
    {
        
        m_SelectPetData = m_manager.GetSelectPetData();
        bool Unlock = false;
        
        if (m_SelectPetData.Lock)
        {
            if (PlayerPrefs.HasKey(m_manager.selectedPetindex.ToString() + "lock"))
            {
                int charlock = PlayerPrefs.GetInt(m_manager.selectedPetindex.ToString() + "lock");
                Unlock = charlock == 0 ? false : true;
            }

        }


        //if (m_SelectPetData.Lock)
        //{
        //    foreach (UnLockJson x in m_playerUnlock.UnLockDatas.UnLockData)
        //    {
        //        if (m_manager.selectedPetindex == x.lockIndex && m_manager.selectedCategory == x.Category)
        //        {
        //            Unlock = true;
        //        }
        //    }
        //}
        else
        {
            Unlock = true;
        }

            
        if(Unlock)
        {
            m_pageCtrl.SwtichPage(PageIndex);
            int ColorCount = m_SelectPetData.Diff_Color.Length;
            Sprite[] ColorSet = m_SelectPetData.Diff_Color;
            m_ColorSelect.GenerateColor(ColorCount, ColorSet);
            m_PetSetting_Data.SelectedPetData(m_SelectPetData);
            BottomMenu.SetActive(false);
            BottomMenuBG.SetActive(false);
        
        }
        else
        {
            Debug.Log("page " + m_manager.selectedPetindex.ToString());
            PlayerPrefs.SetInt("Index", m_manager.selectedPetindex);
            SceneManager.LoadScene("Unlock");
        }
        
    }
    public void Scanned()
    {
        if(m_manager.scanned == true)
        {
            m_manager.scanned = false;
            SelectPetAction();
        }
    }
    public MenuSelector m_MenuSelector;
    public void MenuSelectAction()
    {
        m_MenuSelector.confirmSelectPet();
        //m_pageCtrl.SwtichPage(PageIndex);
    }
    public bool SwicthPage;
    public int PageIndex;


}
