﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UI;


    #if UNITY_EDITOR
[CustomEditor(typeof(MainButtonManager)),CanEditMultipleObjects]

public class MainButtonManagerEditor : Editor
{
    
    int selected = 0;
    public Object source;
    public override void OnInspectorGUI()
    {
        
        MainButtonManager m_target = (MainButtonManager)target;
        string[] options = new string[(int)MainButtonManager.BtnType.LastIndex];
        for (int i = 0; i < options.Length; i++)
        {
            options[i] =((MainButtonManager.BtnType)i).ToString();
        }


        //EditorGUILayout.ObjectField(m_target, typeof(MainButtonManager), false);        //EditorGUI.BeginChangeCheck();
        m_target.m_type = (MainButtonManager.BtnType)EditorGUILayout.Popup("ButtonType", 
                                                                            (int)m_target.m_type, 
                                                                            options); 
        
        if(m_target.m_type == MainButtonManager.BtnType.SelectPet )
        {
            m_target.m_ColorSelect =(ColorSelect)EditorGUILayout.ObjectField("TargetPage", 
                                                                        m_target.m_ColorSelect, 
                                                                        typeof(ColorSelect), true);
            m_target.m_CategorySelector =(Selector)EditorGUILayout.ObjectField("CategorySelector", 
                                                                        m_target.m_CategorySelector, 
                                                                        typeof(Selector), true);
            m_target.m_PetSetting_Data =(PetSetting_Data)EditorGUILayout.ObjectField("PetSetting_Data", 
                                                                        m_target.m_PetSetting_Data, 
                                                                        typeof(PetSetting_Data), true);
            m_target.BottomMenu =(GameObject)EditorGUILayout.ObjectField("BottomMenu", 
                                                                        m_target.BottomMenu, 
                                                                        typeof(GameObject), true);    
            m_target.BottomMenuBG =(GameObject)EditorGUILayout.ObjectField("BottomMenuBG", 
                                                                        m_target.BottomMenuBG, 
                                                                        typeof(GameObject), true);                                                         
        }
        if(m_target.m_type == MainButtonManager.BtnType.MenuSelect )
        {
            m_target.m_MenuSelector =(MenuSelector)EditorGUILayout.ObjectField("MenuSelector", 
                                                            m_target.m_MenuSelector, 
                                                            typeof(MenuSelector), true);
        }
        m_target.SwicthPage = EditorGUILayout.Toggle("SwicthPage",m_target.SwicthPage);
        if(m_target.SwicthPage)
            m_target.PageIndex = EditorGUILayout.IntField("PageIndex",m_target.PageIndex);
    }
    
}
    #endif