﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SwitchPetAnimaCtrl : MonoBehaviour
{
    SelectPetsManager m_manager;
    public GameObject[] PetAnimaCtrls;
    ChangeAnimation[] m_categoryAnima;
    ScrollRectSnap2 m_snap;
    int SeledtedType;
    // Start is called before the first frame update
    void Start()
    {
        m_snap = GetComponentInChildren<ScrollRectSnap2>();
        m_categoryAnima = new ChangeAnimation[PetAnimaCtrls.Length];
        if(SelectPetsManager.instance != null)
            m_manager = SelectPetsManager.instance;

        SeledtedType = (int)m_manager.GetSelectPetData().m_type;
        for (int i = 0; i < PetAnimaCtrls.Length; i++)
        {
            m_categoryAnima[i] = PetAnimaCtrls[i].GetComponent<ChangeAnimation>();
            PetAnimaCtrls[i].SetActive( i == SeledtedType ? true : false);
        }
        m_snap.AssignAnimationList(PetAnimaCtrls[SeledtedType].GetComponent<RectTransform>());
    }
    public ChangeAnimation GetChangeAnimation()
    {
        return m_categoryAnima[SeledtedType];
    }
    public void AssignAnimator(Animator anima)
    {
        m_categoryAnima[SeledtedType].AssignAnimator(anima);
    }
}
