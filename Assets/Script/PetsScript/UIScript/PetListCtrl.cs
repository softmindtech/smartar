﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PetListCtrl : MonoBehaviour
{
    public enum PetCategory
    {
        Cat = 0,
        Garena_Female = 1,

        Dog = 2,
        Garena_male = 3,

        AllPet = 4,
        AllGarena = 5,

    }
    public GameObject GeneratePrefab;

    ScrollRectSnap m_scrollRectSnap;

    public PetCategory m_category;
    
    [SerializeField]
    GameObject[]  Pets;
    CanvasGroup[] PetsCG;
    SelectPetsManager m_manager;
    PlayerUnLockJson m_playerUnlock;
    public int PetsLength {get; private set;} 
    // Start is called before the first frame update
    int Language;

    void Start()
    {
        if(SelectPetsManager.instance != null)
        {
            m_manager = SelectPetsManager.instance;
            m_playerUnlock = m_manager.GetComponent<PlayerUnLockJson>();
        }

        if(PlayerPrefs.HasKey("Language"))
            Language = PlayerPrefs.GetInt("Language");
        else
            Language = 1;

        m_scrollRectSnap = transform.GetComponentInParent<ScrollRectSnap>();

        switch(m_category)
        {
            case PetCategory.Cat:
            case PetCategory.Dog:
            case PetCategory.Garena_Female:
            case PetCategory.Garena_male:
            PetsLength = m_manager.m_petsCategory[(int)m_category].m_Petsmanager.Length;
            break;

            case PetCategory.AllPet:
            PetsLength += m_manager.m_petsCategory[(int)PetCategory.Cat].m_Petsmanager.Length;
            PetsLength += m_manager.m_petsCategory[(int)PetCategory.Dog].m_Petsmanager.Length;
            break;
            case PetCategory.AllGarena:
            PetsLength += m_manager.m_petsCategory[(int)PetCategory.Garena_Female].m_Petsmanager.Length;
            PetsLength += m_manager.m_petsCategory[(int)PetCategory.Garena_male].m_Petsmanager.Length;
            break;

            default:
            break;
        }
        PetsCG = new CanvasGroup[PetsLength];
        Pets = new GameObject[PetsLength];
        GenerateList((int)m_category);
    }

    void GenerateList(int CategoryFlag)
    {
        int index = 0;
        switch(CategoryFlag)
        {
            case 0:
            case 1:
            case 2:
            case 3:
            for (int i = 0; i < m_manager.m_petsCategory[CategoryFlag].m_Petsmanager.Length; i++)
                SpwanPrefab(i, CategoryFlag);
            break;

            // case 4:
            // index = 0;
            // for (int i = (int)PetCategory.Cat; i <= (int)PetCategory.Dog; i++)
            // {
            //     for (int j = 0; j < m_manager.m_petsCategory[i].m_Petsmanager.Length; j++)
            //     {
            //         SpwanPrefab(j, i);
            //         index++;
            //     }
            // }
            // break;

            // case 5:
            // index = 0;
            // for (int i = (int)PetCategory.Garena_Female; i <= (int)PetCategory.Garena_male; i++)
            // {
            //     Debug.Log(i);
            //     for (int j = 0; j < m_manager.m_petsCategory[i].m_Petsmanager.Length; j++)
            //     {
            //         SpwanPrefab(j, i);
            //         index++;
            //     }
            // }
            // break;

            default:
            break;
        }
        
    }

    public void StartPosition(int index)
    {
        m_scrollRectSnap.StartPosition(index);
    }
    RectTransform currentSelection;
    int ListIndex;
    void OnDisable()
    {
        currentSelection = null;
        ListIndex = -1;
    }
    public void Update()
    {

        if(!m_scrollRectSnap.setUpfinish)
            return;

        if(ListIndex !=  m_scrollRectSnap.ListIndex)
            ListIndex = m_scrollRectSnap.ListIndex;

        if(currentSelection != Pets[ListIndex].GetComponent<RectTransform>())
            currentSelection = Pets[ListIndex].GetComponent<RectTransform>();

        float dist = currentSelection.anchoredPosition.x - m_scrollRectSnap.ContentposX;
        
        dist = dist/(m_scrollRectSnap.PetsWidth);
        float alpha = (1 - Mathf.Abs(dist));
        dist = (1 - Mathf.Abs(dist)) * 1.2f;
        dist = Mathf.Clamp(dist, 1, 1.2f);
        Vector3 petScale = new Vector3(dist,dist,dist);
        
        for (int i = 0; i < Pets.Length; i++)
        {
           //m_manager.selectedPetindex = ListIndex;
            Vector3 scale = i == ListIndex ? petScale : new Vector3(1,1,1);
            Pets[i].transform.localScale = Vector3.Lerp(Pets[i].transform.localScale, scale, Time.deltaTime * 20);
            float alpha_buffer = i == ListIndex ? alpha : 0.5f;
            PetsCG[i].alpha = Mathf.Lerp(PetsCG[i].alpha, alpha_buffer, Time.deltaTime * 4);
        }
    }

    int spwanIndex = 0;
    void SpwanPrefab(int index, int category)
    {
        
        GameObject buffer = Instantiate(GeneratePrefab, transform.position, Quaternion.identity);
        PetListObj m_PetListObj = buffer.GetComponent<PetListObj>();
        
        m_PetListObj.m_data = new PetListObj.objData(category, index);
        PetManager.PetData m_PetData = m_manager.m_petsCategory[category].m_Petsmanager[index].m_petDetail;
        if(Language == 0)
            m_PetListObj.PetDetail(m_PetData.MenuPhoto, m_PetData.name, m_PetData.Variety);
        else
            m_PetListObj.PetDetail(m_PetData.MenuPhoto, m_PetData.c_name, m_PetData.c_variety);

        if(m_PetData.Lock)
        {
            //if(m_playerUnlock.UnLockDatas.UnLockData.Count != 0)
            //{
            //    bool match = false;
            //    foreach(UnLockJson x in m_playerUnlock.UnLockDatas.UnLockData)
            //    {
            //        if(index == x.lockIndex && category == x.Category)
            //        {
            //            match = true;
            //        }
            //    }
            //    if(!match)
            //        m_PetListObj.Lock();
            //}
            
            if (PlayerPrefs.HasKey(index.ToString() + "lock"))
            {
                
                bool islock = PlayerPrefs.GetInt(index.ToString() + "lock") == 0 ? true : false;

                if (islock)
                    m_PetListObj.Lock();
            }
            else
            {
                PlayerPrefs.SetInt(index + "lock" , 0);
                m_PetListObj.Lock();
            }
        }

        buffer.transform.parent = this.transform;
        Pets[spwanIndex]  = buffer;
        PetsCG[spwanIndex] = buffer.GetComponent<CanvasGroup>();
        spwanIndex ++;
    }

    public PetListObj.objData GetSelectPet()
    {
        return Pets[ListIndex].GetComponent<PetListObj>().m_data;
    }
}
