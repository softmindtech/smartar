﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Selector : MonoBehaviour
{
    public enum SelectType
    {
        Pet = 0,
        Garena = 2,

    }

    public SelectType m_selectType;

    public Button[] m_btns;
    public GameObject[] List;
    public float[] offset;
    PetListCtrl[] m_ListPetsListCtrl;
    SelectPetsManager m_manager;
    CanvasScalerHelper m_canvasScaler;
    public ScrollRectSnap m_scrollRectSnap;

    Text[] m_btn_text;
    public Image selectorUp;
    public Image selectorDown;
    public int selectedList;

    public int startIndex ;

    int Language;
    // Start is called before the first frame update
    void Awake()
    {   
        m_ListPetsListCtrl = new PetListCtrl[List.Length];

        for (int i = 0; i < List.Length; i++)
        {
            m_ListPetsListCtrl[i] = List[i].GetComponent<PetListCtrl>();
        }

        if(PlayerPrefs.HasKey("Language"))
            Language = PlayerPrefs.GetInt("Language");
        else
            Language = 1;

        m_btn_text = new Text[m_btns.Length];
        for (int i = 0; i < m_btns.Length; i++)
        {
            
            m_btn_text[i] = m_btns[i].GetComponentInChildren<Text>();
            int x = i;
            Debug.Log(x);
            m_btns[i].onClick.AddListener (() => selection(x));            
        }
    }
    void OnEnable()
    {
        // for (int i = 0; i < List.Length; i++)
        // {
        //     if(m_btns[i].gameObject.tag ==  m_selectType.ToString())
        //         m_btns[i].gameObject.SetActive(true);
        //     else
        //         m_btns[i].gameObject.SetActive(false);
        // }
        // //startIndex = (int)m_selectType;
 
        // StartCoroutine(delay());
    }
    void Start()
    {
        

        if(SelectPetsManager.instance != null)
            m_manager = SelectPetsManager.instance;

        if(CanvasScalerHelper.instance != null)
            m_canvasScaler = CanvasScalerHelper.instance;
        


        //startIndex = (int)m_selectType;
        StartCoroutine(delay());
        
    }
    IEnumerator delay()
    {
        if(m_manager !=null)
        startIndex = m_manager.selectedCategory;
        yield return new WaitForSeconds(0.01f);
        for (int i = 0; i < List.Length; i++)
            List[i].SetActive(false);
        selection(startIndex);
        yield return new WaitForSeconds(0.5f);
        m_ListPetsListCtrl[startIndex].StartPosition(m_manager.selectedPetindex);
    }

    void selection(int index)
    {
        selectedList = index;
        selectorUp.transform.position =  m_btns[index].transform.position + new Vector3(0, offset[index + Language * 2] * m_canvasScaler.transform.transform.localScale.x, 0);
        selectorDown.transform.position =  m_btns[index].transform.position + new Vector3(0, -offset[index + Language * 2] * m_canvasScaler.transform.transform.localScale.x, 0);
        for (int i = 0; i < m_btn_text.Length; i++)
        {
            m_btn_text[i].color = i == index ? new Color(0.2f,0.2f,0.2f) : new Color(0.8f,0.8f,0.8f);
        }
        RectTransform selectedList_rect = List[index].GetComponent<RectTransform>();
        m_scrollRectSnap.ChangePetsList(selectedList_rect, m_ListPetsListCtrl[index].PetsLength);
        
    }

    public void confirmSelectPet()
    {
        PetListObj.objData seletedPet = m_ListPetsListCtrl[selectedList].GetSelectPet();
        m_manager.selectedCategory = (int)seletedPet.m_category;
        m_manager.selectedPetindex = seletedPet.index;
    }
}
