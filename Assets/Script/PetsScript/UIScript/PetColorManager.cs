﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PetColorManager : MonoBehaviour
{
    [SerializeField]
    Image[] m_Colors;
    int TextureIndex = 0;
    public Action<int> ChangePetTexsture;
    bool setup = false;
    public GameObject ColorBtnPrefab;
    List<MainButtonManager> m_ColorBtns = new List<MainButtonManager>();

    // Start is called before the first frame update
    void OnEnable()
    {
        // if(setup)
        //     return;


        // Image[] buffer = GetComponentsInChildren<Image>();
        // m_Colors = new Image[buffer.Length - 1];
        // int m_Colorindex = 0;
        // for (int i = 0; i < buffer.Length; i++)
        // {
        //     if(buffer[i].gameObject != this.gameObject)
        //     {
        //         m_Colors[m_Colorindex] = buffer[i];
        //         m_Colorindex ++;
        //     }
        // }
        // MainButtonManager[] m_buttons = GetComponentsInChildren<MainButtonManager>();
        // for (int i = 0; i < m_buttons.Length; i++)
        // {
        //     m_buttons[i].Colorindex = i;
        //     m_buttons[i].SelectColorAction = ColorBtn;
        // }
        // setup = true;
    }

    public void ColorBtn(int index)
    {
        TextureIndex = index;
        if(ChangePetTexsture != null)
            ChangePetTexsture(TextureIndex);
    }

    void SpwanObj(int index)
    {
        GameObject buffer = Instantiate(ColorBtnPrefab, transform.position, Quaternion.identity);
        buffer.transform.parent = this.transform;
        MainButtonManager m_ColorBtn = buffer.GetComponent<MainButtonManager>();
        //m_ColorBtn.Colorindex = index;
        //m_ColorBtn.SelectColorAction = ColorBtn;
        m_ColorBtns.Add(m_ColorBtn);
    }
    public void SetColor(Color[] petColor)
    {   

        if(m_ColorBtns.Count == 0 )
            for (int i = 0; i < petColor.Length; i++)
                SpwanObj(i);

        else if(m_ColorBtns.Count < petColor.Length)
        {
            int l =  petColor.Length - m_ColorBtns.Count;
            for (int i = 0; i < l; i++)
            {
                int x = i + m_ColorBtns.Count;
                SpwanObj(x);
            }
            
        }

        int index = 0;
        foreach(MainButtonManager x in m_ColorBtns)
        {
            x.gameObject.SetActive(index < petColor.Length ? true : false);
            if(index < petColor.Length)
                //x.SetImageColor(petColor[index]);
            index ++;
        }

        // for (int i = 0; i < petColor.Length; i++)
        // {
        //     m_Colors[i].color = petColor[i];
        // }
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
