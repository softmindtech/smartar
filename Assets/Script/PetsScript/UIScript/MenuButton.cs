﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class MenuButton : ButtonClass
{
    
    public enum BtnType
    {
        GarenaAR = 0,
        PetAr = 1,
        Test = 2,
        LastIndex = 3
    }

    public BtnType m_type;

    public Action ButtonAction;

    public override void ButtonClick()
    {
        
        Debug.Log("Click");
        if(ButtonAction !=null)
            ButtonAction();
    }

    void Start()
    {
        SetUp();
        switch (m_type)
        {
            case BtnType.GarenaAR:
            case BtnType.PetAr:
            ButtonAction += ARmode;
            break;
            case BtnType.Test:
            ButtonAction += Test;
            break;
            default:
            break;
        }

    }

    public Selector ChoosePetSelector;

    void ARmode()
    {
        if(m_type == BtnType.GarenaAR)
        {
            ChoosePetSelector.m_selectType = Selector.SelectType.Garena;
        }
        else if(m_type == BtnType.PetAr)
        {
            ChoosePetSelector.m_selectType = Selector.SelectType.Pet;
        }
        m_pageCtrl.SwtichPage(1);
    }

    void Test()
    {
        Debug.Log("TEst");
    }
    
}
