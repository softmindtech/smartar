﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ScrollRectSnap2 : MonoBehaviour
{

    public float ContentposX{get; private set;}
    RectTransform AnimationList;
    RectTransform ContentRect;

    Vector2 SnapPos;
    ChangeAnimation changeAnimaScript;
    ScrollRect m_scrollRect;

    public Action<int> BtnAction;
    float listWidth;
    float buttonWith;
    float offset;
    public int ListCount = 5;
    public int buttonActionIndex {get; private set;}
    // Start is called before the first frame update
    void Start()
    {
        m_scrollRect = GetComponent<ScrollRect>();
        ContentRect = transform.GetChild(0).GetComponent<RectTransform>();
        m_scrollRect.onValueChanged.AddListener(CheckSnapPos);
    }

    public void AssignAnimationList(RectTransform list)
    {
        AnimationList = list;
        changeAnimaScript = AnimationList.GetComponent<ChangeAnimation>();
        changeAnimaScript.m_scrollRectSnap=this;
        changeAnimaScript.scrollsanp += Snap;
        BtnAction += changeAnimaScript.OnButtonClick;
        listWidth = AnimationList.rect.width;
        buttonWith = listWidth/ListCount;
        offset = listWidth/2 - buttonWith/2;

    }
    // Update is called once per frame
    public float relativePos{get; private set;}
    bool move = false;
    bool snap = false;
    void Update()
    {
        ContentposX = ContentRect.anchoredPosition.x;

        
        relativePos = -ContentposX + offset + buttonWith/2;
        
        int x = (int)(relativePos/buttonWith);
        buttonActionIndex = x >= ListCount ? ListCount -1 : x;

        if(Input.touchCount == 0)
            return;
        
        Touch touchzero = Input.GetTouch(0);

        if(touchzero.phase == TouchPhase.Began || touchzero.phase == TouchPhase.Moved)
        {
            move = true;
            snap = false;
        }
        else if(touchzero.phase == TouchPhase.Ended)
        {
            move = false;
        }
    }

    void CheckSnapPos(Vector2 refV2)
    {
        if(move == false && !snap)
        {

            if(relativePos >  listWidth || relativePos < 0)
            {
                buttonActionIndex = relativePos >  listWidth ? ListCount - 1 : 0;
                float snapPosx =  relativePos >  listWidth ? -(buttonWith * (ListCount -1)) + offset : offset;
                SnapPos = new Vector2 (snapPosx , ContentRect.anchoredPosition.y);
                StartCoroutine(Snap());
            }
            else if(relativePos <= listWidth && relativePos >= 0)
            {
                for (int i = 0; i < ListCount; i++)
                {
                    if(relativePos > buttonWith * i && relativePos < buttonWith * (i +1))
                    {
                        buttonActionIndex = i;
                        float snapPosx = -(buttonWith * i) + offset;
                        SnapPos = new Vector2 (snapPosx, ContentRect.anchoredPosition.y);
                        StartCoroutine(Snap());
                    }
                }
            }

            if(BtnAction != null)
                BtnAction(buttonActionIndex);

            snap = true;
        }
    }
    void Snap(int index)
    {
        float snapPosx = -(buttonWith * index) + offset;
        SnapPos = new Vector2 (snapPosx, ContentRect.anchoredPosition.y);
        if(BtnAction != null)
            BtnAction(index);
        StartCoroutine(Snap());
    }
    IEnumerator Snap()
    {
        m_scrollRect.enabled = false;
        float elaspeTime = 0;
        float waitTime = 0.2f;
        
        while(elaspeTime < waitTime)
        {
            ContentRect.anchoredPosition = Vector2.Lerp(ContentRect.anchoredPosition, SnapPos, elaspeTime / waitTime);
            elaspeTime += Time.deltaTime;
            yield return new WaitForFixedUpdate();
        }
        
        m_scrollRect.enabled = true;
    }
}
