﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class ChangeAnimation : MonoBehaviour
{
    public enum ModelType
    {
        Cat,
        Garena,
        Dog
    }
    public ModelType modelType;
    public Animator Pet;
    PetAnimationCtrl m_animatorCtrl;
    
    Image[] m_Image;
    Button[] m_btns;
    int selectedBtn = 0;
    public int[] AnimationIndex;
    
    public Action<int> ChangeAnimationAction;
    public Action<int> scrollsanp;
    // Start is called before the first frame update
    void Start()
    {
        m_Image = GetComponentsInChildren<Image>();
        
        m_btns = new Button[m_Image.Length];
        for (int i = 0; i < m_Image.Length; i++)
        {
            m_btns[i] = m_Image[i].GetComponent<Button>();
            int x = i;
            m_btns[i].onClick.AddListener(() => snap(x));
        }
    }
    public void snap(int index)
    {
        if(scrollsanp != null)
            scrollsanp(index);
    }
    public void AssignAnimator(Animator m_animator)
    {
        Pet = m_animator;
        m_animatorCtrl = Pet.GetComponent<PetAnimationCtrl>();
    }
    public void OnButtonClick(int index)
    {
        //StartCoroutine(scaleSelectBtn(index));
        if(Pet == null)
            return;
        if(AnimationIndex[index] != 4)
        {
            Pet.SetTrigger("BaseAnima");
            if(modelType == ModelType.Garena)
               StartCoroutine(delayAction());
        }
        else
        {
            Pet.SetTrigger("Walk");
            if(modelType != ModelType.Garena)
            {
                Quaternion rot = Quaternion.Euler(0,0,0);
                Pet.transform.localRotation = rot;
            }
        }

        m_animatorCtrl.ActionEnd();
        Pet.SetFloat("Action", AnimationIndex[index]);
        //StartCoroutine(ButtonScaleTransition(index));
        if(ChangeAnimationAction != null)
            ChangeAnimationAction(AnimationIndex[index]);
    }
    // Update is called once per frame
    int ListIndex;
    RectTransform currentSelection;
    public ScrollRectSnap2 m_scrollRectSnap;

    IEnumerator delayAction()
    {
        yield return new WaitForSeconds(0.1f);
         Pet.SetTrigger("Interactive");
    }
    public void scaleSelectBtn()
    {

        if(ListIndex !=  m_scrollRectSnap.buttonActionIndex)
            ListIndex = m_scrollRectSnap.buttonActionIndex;

        if(currentSelection != m_Image[ListIndex].GetComponent<RectTransform>())
            currentSelection = m_Image[ListIndex].GetComponent<RectTransform>();
        
        float dist = currentSelection.anchoredPosition.x - m_scrollRectSnap.relativePos;
        dist = dist/(m_scrollRectSnap.ListCount);
        dist = (1 - Mathf.Abs(dist)) * 1.4f;
        dist = Mathf.Clamp(dist, 1,1.4f);
        Vector3 petScale = new Vector3(dist,dist,dist);
        
        for (int i = 0; i < m_Image.Length; i++)
        {
            Vector3 scale = i == ListIndex ? petScale : new Vector3(1,1,1);
            m_Image[i].transform.localScale = Vector3.Lerp(m_Image[i].transform.localScale, scale, Time.deltaTime * 10);
        }
    }

    void Update()
    {
        scaleSelectBtn();
    }

}
