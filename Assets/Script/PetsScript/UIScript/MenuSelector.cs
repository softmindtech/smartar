﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MenuSelector : MonoBehaviour
{
    public Button[] m_btns;
    public GameObject[] List;
    MenuListCtrl[] m_ListMenuListCtrl;
    SelectPetsManager m_manager;
    public ScrollRectSnap_Menu m_scrollRectSnap;

    Image[] m_btn_image;
    Text[] m_btn_text;
    public Image selector;
    public int selectedList;

    public int startIndex = 0;
    // Start is called before the first frame update
    void Awake()
    {   
        m_ListMenuListCtrl = new MenuListCtrl[List.Length];

        for (int i = 0; i < List.Length; i++)
        {
            m_ListMenuListCtrl[i] = List[i].GetComponent<MenuListCtrl>();
        }
    }
    
    void Start()
    {

        m_btn_image = new Image[m_btns.Length];
        m_btn_text = new Text[m_btns.Length];

        if(SelectPetsManager.instance != null)
            m_manager = SelectPetsManager.instance;


        //startIndex = m_manager.selectedCategory;

        for (int i = 0; i < m_btns.Length; i++)
        {
            m_btn_image[i] = m_btns[i].GetComponent<Image>();
            m_btn_text[i] = m_btns[i].GetComponentInChildren<Text>();
            Vector3 pos = m_btn_image[i].transform.position;
            int x = i;
            m_btns[i].onClick.AddListener (() => selection(x));            
        }

        StartCoroutine(delay());
        
    }
    IEnumerator delay()
    {
        yield return new WaitForSeconds(0.01f);
        for (int i = 0; i < List.Length; i++)
            List[i].SetActive(false);
        selection(startIndex);
        yield return new WaitForSeconds(0.5f);
        m_ListMenuListCtrl[startIndex].StartPosition(m_manager.selectedPetindex);
    }

    void selection(int index)
    {
        selectedList = index;
        selector.transform.position =  m_btn_image[index].transform.position ;
        for (int i = 0; i < m_btn_text.Length; i++)
        {
            m_btn_text[i].color = i == index ? new Color(0.2f,0.2f,0.2f) : new Color(0.8f,0.8f,0.8f);
        }
        RectTransform selectedList_rect = List[index].GetComponent<RectTransform>();
        m_scrollRectSnap.ChangePetsList(selectedList_rect, m_ListMenuListCtrl[index].MenuLength);
        
    }

    public void confirmSelectPet()
    {
        m_scrollRectSnap.ClickAction();
    }
}
