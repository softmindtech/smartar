﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BottomBtnCtrl : MonoBehaviour
{
    Button[] m_btns;
    BottomBtnObj[] m_bottomObj;
    PageCtrl m_pagerCtrl;
    public Sprite[] SelectedSprite;


    // Start is called before the first frame update
    void Start()
    {
        m_pagerCtrl = GetComponentInParent<PageCtrl>();
        m_btns = GetComponentsInChildren<Button>();
        m_bottomObj = GetComponentsInChildren<BottomBtnObj>();

        m_btns[0].onClick.AddListener(() => m_pagerCtrl.SwtichPage(1));
        m_btns[1].onClick.AddListener(() => m_pagerCtrl.SwtichPage(3));
        //m_btns[2].onClick.AddListener(() => m_pagerCtrl.SwtichPage(3));
        for (int i = 0; i < m_btns.Length; i++)
        {
            int flag = i;
            m_btns[i].onClick.AddListener(() =>Select(flag));
        }
        Select(0);
    }

    // Update is called once per frame

    public void Select(int index)
    {
        m_btns[index].GetComponent<Image>().sprite = SelectedSprite[index];
        for (int i = 0; i < m_btns.Length; i++)
        {
            if(i != index)
                m_bottomObj[i].Deselect();
        }

    }

}
