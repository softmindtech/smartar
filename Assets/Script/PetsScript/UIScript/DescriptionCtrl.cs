﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class DescriptionCtrl : MonoBehaviour
{

    Text m_text;
    int defScrennHigh = 1920;
    int def_size = 35;
    int cur_screenHight;
    float scalefactor;
    // Start is called before the first frame update
    void Start()
    {
        m_text = GetComponent<Text>();
        cur_screenHight = Screen.height;
        scalefactor = (float)cur_screenHight /  (float)defScrennHigh;

        m_text.resizeTextMinSize = (int)(def_size * scalefactor);
    }


}
