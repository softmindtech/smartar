﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LoginPageLanguageChange : MonoBehaviour
{
    Text m_text;
    Button m_btn;
    int language;
    // Start is called before the first frame update

    void Start()
    {
        
        m_text = GetComponentInChildren<Text>();
        m_btn = GetComponent<Button>();
        if(PlayerPrefs.HasKey("Language"))
            language = PlayerPrefs.GetInt("Language");
        else
            language = 1;

        if(language == 0)
        {
            m_text.text ="中文";
            m_btn.onClick.AddListener(() => RestartApp(0));
        }
        else
        {
            m_text.text ="Eng";
            m_btn.onClick.AddListener(() => RestartApp(1));
        }

    }

    public void RestartApp(int inputvalue)
    {
        if(inputvalue == 1)
            PlayerPrefs.SetInt("Language", 0);
        else
            PlayerPrefs.SetInt("Language", 1);
        Scene acrtivescene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(acrtivescene.name);
    }
}
