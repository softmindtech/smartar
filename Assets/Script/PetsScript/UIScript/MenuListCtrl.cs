﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuListCtrl : MonoBehaviour
{

    ScrollRectSnap_Menu m_scrollRectSnap;
    GameObject[]  MenuObj;
    MenuButton[] MenuObjBtns;
    CanvasGroup[] MenuObjsCG;
    public int MenuLength {get; private set;} 
    // Start is called before the first frame update
    void Start()
    {
        m_scrollRectSnap = transform.GetComponentInParent<ScrollRectSnap_Menu>();
        MenuLength = transform.childCount;
        MenuObj = new GameObject[MenuLength];
        MenuObjBtns = new MenuButton[MenuLength];
        MenuObjsCG = new CanvasGroup[MenuLength];
        for (int i = 0; i < MenuLength; i++)
        {
            MenuObj[i] = transform.GetChild(i).gameObject;
            MenuObjBtns[i] = MenuObj[i].GetComponent<MenuButton>();
            MenuObjsCG [i] = MenuObj[i].GetComponent<CanvasGroup>();
        }
    }

    public void StartPosition(int index)
    {
        m_scrollRectSnap.StartPosition(index);
    }
   RectTransform currentSelection;
    int ListIndex;
    void OnDisable()
    {
        currentSelection = null;
        ListIndex = -1;
    }
    public void Update()
    {

        if(!m_scrollRectSnap.setUpfinish)
            return;

        if(ListIndex !=  m_scrollRectSnap.ListIndex)
            ListIndex = m_scrollRectSnap.ListIndex;

        if(currentSelection != MenuObj[ListIndex].GetComponent<RectTransform>())
            currentSelection = MenuObj[ListIndex].GetComponent<RectTransform>();

        float dist = currentSelection.anchoredPosition.x - m_scrollRectSnap.ContentposX;
        dist = dist/(m_scrollRectSnap.PetsWidth);
        float alpha = (1 - Mathf.Abs(dist));
        dist = (1 - Mathf.Abs(dist)) * 1.1f;
        dist = Mathf.Clamp(dist, 1,1.1f);
        Vector3 petScale = new Vector3(dist,dist,dist);
        
        for (int i = 0; i < MenuObj.Length; i++)
        {
            Vector3 scale = i == ListIndex ? petScale : new Vector3(1,1,1);
            MenuObj[i].transform.localScale = Vector3.Lerp(MenuObj[i].transform.localScale, scale, Time.deltaTime * 10);
            float alpha_buffer = i == ListIndex ? alpha : 0.5f;
            MenuObjsCG[i].alpha = Mathf.Lerp(MenuObjsCG[i].alpha, alpha_buffer, Time.deltaTime * 2);
        }
    }

    int spwanIndex = 0;
    
    public void ClickButtonAction(int index)
    {
        MenuObjBtns[index].ButtonClick();
    }
    public PetListObj.objData GetSelectPet()
    {
        return MenuObj[ListIndex].GetComponent<PetListObj>().m_data;
    }
}
