﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class PreviewImage : MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler
{
    // Start is called before the first frame updatebool isPointer;
    public GameObject PreviewGameObject;
    Vector2 pressPos;
    bool isPointer;
    public void OnPointerDown(PointerEventData eventData)
    {
        isPointer = true;
        pressPos = eventData.position;

    }
    public void OnPointerUp(PointerEventData eventData)
    {
        isPointer = false;
    }
    public void OnDrag(PointerEventData eventData)
    {
        if(isPointer)
        {
            Vector2 delta = eventData.position - pressPos;
            pressPos = eventData.position;
            PreviewGameObject.transform.Rotate(0, -delta.x * Time.deltaTime * 10, 0);
        }
    }
    
    private bool IsPointerOverUIObject() 
    {
      PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
      eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
      List<RaycastResult> results = new List<RaycastResult>();
      EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
      return results.Count > 0;
  }
}
