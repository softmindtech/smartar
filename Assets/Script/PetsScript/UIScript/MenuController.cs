﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MenuController : MonoBehaviour
{

    public static MenuController instacene;
    public GameObject PreviousButton;
    public GameObject[] Pages;
    public GameObject ConfirmBtn;

    Stack CurrentPage = new Stack();
    // Start is called before the first frame update

    void Awake()
    {
        if(instacene == null)
            instacene = this;
    }
    void Start()
    {
        SetUP();
        GoToPage(Pages[0]);
    }
    void SetUP()
    {
        for (int i = 1; i < Pages.Length; i++)
        {
            Pages[i].SetActive(false);
        }
    }

    public void GoToPage(GameObject page)
    {
        page.SetActive(true);
        CurrentPage.Push(page);
        CheckPage(page.name);
        
    }

    public void PreviousPage()
    {
        GameObject page = CurrentPage.Pop() as GameObject;
        CheckPage((CurrentPage.Peek() as GameObject).name);
        page.SetActive(false);
    }

    public void BackToHomePage()
    {
        while(CurrentPage.Peek() as GameObject != Pages[0])
        {
            PreviousPage();
        }
    }

    void CheckPage(string pagename)
    {
        PreviousButton.SetActive(CurrentPage.Count > 1 ? true : false);
        ConfirmBtn.SetActive(pagename == "Content_Selection1_P3" ? true : false);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
