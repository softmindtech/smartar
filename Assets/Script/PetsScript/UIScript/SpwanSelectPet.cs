﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;


public class SpwanSelectPet : MonoBehaviour
{
    SelectPetsManager m_selectedPetManager;
    
    PetManager.PetData m_petData;
    public ARPlaceOnPlane m_ARPlace;
    public SwitchPetAnimaCtrl m_changeAnimation;
    public Button resetButton;
    
    public GameObject BlockUIInteractive;
    GameObject pet;
    // Start is called before the first frame update
    void Awake()
    {
        BlockUIInteractive.SetActive(true);
    }
    async void Start()
    {

        if(SelectPetsManager.instance != null)
            m_selectedPetManager = SelectPetsManager.instance;
        
        m_petData = m_selectedPetManager.GetSelectPetData();

        //spwan
        Quaternion placeAngle = Quaternion.Euler(0,180,0);
        pet = Instantiate(m_petData.FourFootPrefab, transform.position, placeAngle);
        
        pet.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        Animator m_anima = pet.GetComponent<Animator>();
        PetBlendShapeCtrl m_blendShapeCtrl = pet.GetComponentInChildren<PetBlendShapeCtrl>();
        PetAnimationCtrl m_petAnimCtrl = pet.GetComponent<PetAnimationCtrl>();

        //set blendshape and texture
        int TextureIndex = m_selectedPetManager.ColorSelect;
        if(m_blendShapeCtrl != null)
        {
            m_blendShapeCtrl.ChangeMaterialTexture( m_petData.Diff_Texture[TextureIndex]);
            float blendshapeValue = m_selectedPetManager.sizeValue;
            m_blendShapeCtrl.SetBlendShapeVale(blendshapeValue);
        }

        //assign to other script
        m_changeAnimation.AssignAnimator(m_anima);
        m_ARPlace.AssignModel(pet, m_selectedPetManager.selectedCategory, m_changeAnimation);
        

        //resetButton.onClick.AddListener(()=>reset());
        await Task.Delay(1500);
        BlockUIInteractive.SetActive(false);
        
    }

    public void reset()
    {
        if(pet.activeSelf)
            pet.SetActive(false);
        m_ARPlace.Reset();
    }   

}
