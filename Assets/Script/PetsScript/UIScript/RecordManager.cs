﻿using UnityEngine;
using NatSuite.Recorders;
using NatSuite.Recorders.Inputs;
using NatSuite.Recorders.Clocks;
using UnityEngine.XR.ARFoundation;
using System;
using System.Collections;
using UnityEngine.UI;


public class RecordManager : MonoBehaviour
{
	public ARCameraManager m_CameraManager;
	public GameObject canvas;
	private IMediaRecorder recorder;
	private CameraInput cameraInput;
	public CanvasGroup FinishRecordImage;
	
	private static string FOLDER_NAME = "SmartAR";
	void Start()
	{
		FinishRecordImage.alpha = 0;
	}
	public void CapturePhoto() {
		StopAllCoroutines();
		StartCoroutine(DoCapturePhoto());
		FinishRecordImage.alpha = 1;
		StartCoroutine(fadeOutFinishImage());
	}

	private IEnumerator DoCapturePhoto() {
		canvas.SetActive(false);
		yield return new WaitForEndOfFrame();

		Texture2D screenShot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
		screenShot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0, true);
		screenShot.Apply();
		canvas.SetActive(true);

		string filename = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + "_{0}.jpg";
		NativeGallery.SaveImageToGallery(screenShot, FOLDER_NAME, filename, (error) => {
			Debug.Log("save image error: " + error);
		});
		
	}

	IEnumerator fadeOutFinishImage()
	{
		yield return new WaitForSeconds(1.5f);
		float elapsedTime = 0;
		float waitTime = 5f;
		while (elapsedTime < waitTime)
		{
			FinishRecordImage.alpha = Mathf.Lerp(FinishRecordImage.alpha, 0, elapsedTime/waitTime);
			elapsedTime += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}
		FinishRecordImage.alpha = 0;

	}
	bool runOnce = false;

	public void StartRecording() {
		// Start recording
		var isPortrait = Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown;
		//var videoWidth = isPortrait ? m_CameraManager.cameraMaterial.mainTexture.height : m_CameraManager.cameraMaterial.mainTexture.width;
		//var videoHeight = isPortrait? m_CameraManager.cameraMaterial.mainTexture.width: m_CameraManager.cameraMaterial.mainTexture.height;
		var videoWidth = Screen.width;
		var videoHeight = Screen.height;
		var camera = m_CameraManager.GetComponent<Camera>();

		var frameRate = 30;
		var sampleRate = 0; // no audio
		var channelCount = 0; // no audio
		var clock = new RealtimeClock();
		recorder = new MP4Recorder(videoWidth, videoHeight, frameRate, sampleRate, channelCount);
		// Create recording inputs
		cameraInput = new CameraInput(recorder, clock, camera);

		
		runOnce = true;
	}

	public async void StopRecording() {
		cameraInput.Dispose();
		var path = await recorder.FinishWriting();
		// Playback recording
		Debug.Log($"Saved recording to: {path}");
		//var prefix = Application.platform == RuntimePlatform.IPhonePlayer ? "file://" : "";
		//Handheld.PlayFullScreenMovie($"{prefix}{path}");

		string filename = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + "_{0}.mp4";
		NativeGallery.SaveVideoToGallery(path, FOLDER_NAME, filename, (error) => {
			Debug.Log("save video error: " + error);
		});

		if(runOnce)
		{
			StopAllCoroutines();
			FinishRecordImage.alpha = 1;
			StartCoroutine(fadeOutFinishImage());
			runOnce = false;
		}
	}
}
