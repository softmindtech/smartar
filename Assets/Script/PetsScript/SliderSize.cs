﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SliderSize : MonoBehaviour
{

    Slider m_slider;
    SelectPetsManager m_manager;
    public Action<float> asignBlendShape;

    // Start is called before the first frame update
    void Start()
    {
        if(SelectPetsManager.instance != null)
            m_manager = SelectPetsManager.instance;
        m_slider = GetComponent<Slider>();
        m_slider.onValueChanged.AddListener(returnValueToManager);
    }
    void OnEnable()
    {
        if(m_slider != null)
        {
            m_slider.value = 0;
            returnValueToManager(m_slider.value);
        }
    }
    
    void returnValueToManager(float value)
    {
        m_manager.sizeValue = m_slider.value;
        if(asignBlendShape != null)
            asignBlendShape(value);
    }
}
