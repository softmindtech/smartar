﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UploadSelectPetData : MonoBehaviour
{
    SelectPetsManager m_selectedPet;
    public GAtrack m_Gatrack;
    Button m_btn;
    int Language;
    // Start is called before the first frame update
    void Start()
    {
        if(SelectPetsManager.instance != null)
            m_selectedPet = SelectPetsManager.instance;

        m_btn = GetComponent<Button>();
        m_btn.onClick.AddListener(onButtonClick);

        if(PlayerPrefs.HasKey("Language"))
            Language = PlayerPrefs.GetInt("Language");
        else
            Language = 1;
    }

    void onButtonClick()
    {
        PetManager.PetData petData = m_selectedPet.GetSelectPetData();


        m_Gatrack.LogEventPets(petData.Variety, petData.m_petColorName[m_selectedPet.ColorSelect].ToString());


       // if(!PlayerPrefs.HasKey("UploadLanguage"))
       if (PlayerPrefs.GetInt("UploadLanguage") != Language)
        {
            m_Gatrack.LogEvent(Language == 1 ? "Chinese" : "English");
            PlayerPrefs.SetInt("UploadLanguage", Language);
        }
            
       // Debug.Log(eventName);
        
    }
}
