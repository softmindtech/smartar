﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class ARPlaceOnPlane : MonoBehaviour
{


    [HideInInspector]
    public GameObject Model;

    public ARtutorial m_tutorial;
    //public ParticleSystem HeartEffect;
    
    public Transform[] InteractiveProp;
    
    public micophoneCtrl m_micophone;
    public RectTransform TouchMoveImage;

    Button m_micophone_btn;
    ChangeAnimation ActiveChangeAnimationBTNs;
    Camera m_cam;
    Animator modelAnimator;
    ARRaycastManager m_ARRaycast;
    PetAnimationCtrl m_petAnimCtrl;
    List<ARRaycastHit> hits;
    ARPlaneManager m_PlaneManager;  
    OrientationChange m_orientchange;

    int animationIndex;
    float minScale = 0.05f;
    int petCategory;

    float maxScale = 0.3f;
    [HideInInspector]
    public int rotIndex = 1;
    [HideInInspector]
    public int moveindex = 1;
    
    // Start is called before the first frame update

    public void SetMinMaxScale(float min, float max)
    {
        minScale = min;
        maxScale = max;
    }
    void Start()
    {
        m_orientchange = GetComponent<OrientationChange>();
        m_ARRaycast = GetComponent<ARRaycastManager>();
        m_PlaneManager = GetComponent<ARPlaneManager>();
        m_micophone_btn = m_micophone.GetComponent<Button>();
        
        hits = new List<ARRaycastHit>();
        m_cam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        
        
        m_micophone.orderAction += OrderAction;
        TouchMoveImage.gameObject.SetActive(false);
        m_orientchange.model = Model;
    }
    public void AssignModel(GameObject m_model, int category, SwitchPetAnimaCtrl m_SwitchPetAnimaCtrl)
    {
        ActiveChangeAnimationBTNs = m_SwitchPetAnimaCtrl.GetChangeAnimation();
        ActiveChangeAnimationBTNs.ChangeAnimationAction += changeAnimation;
        Model = m_model;
        
        petCategory = category;
        m_petAnimCtrl = Model.GetComponent<PetAnimationCtrl>();
        modelAnimator = Model.GetComponent<Animator>();
        modelSize = Model.transform.localScale;
        // InteractiveProp[0].transform.parent = m_model.transform;
        // InteractiveProp[1].transform.parent = m_model.transform;
        //Model.SetActive(false);
    }
    public void changeAnimation(int index)
    {
        animationIndex = index;
    }
    
    void Update()
    {
        ARInput();
        detectARPlane();
        
        m_micophone_btn.interactable = Model.activeInHierarchy ;

        
        #region TestArea
        #if UNITY_EDITOR
        // if(Input.GetKeyDown(KeyCode.A))
        // {
        //     modelAnimator.SetTrigger("Interactive");
        //     if(animationIndex == 2)
        //     {
        //         m_petAnimCtrl.endAction = EndAction_DisableButterfly;
        //         InteractiveProp[0].gameObject.SetActive(true);
        //     }
        // }
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        Vector3 ScreenPoint = m_cam.WorldToScreenPoint(Model.transform.position);
        ScreenPoint += new Vector3(h , v, 0) *  10;
        ScreenPoint.x = Mathf.Clamp(ScreenPoint.x, Screen.width - Screen.width * 0.9f, Screen.width * 0.9f);
        ScreenPoint.y = Mathf.Clamp(ScreenPoint.y, Screen.height - Screen.height * 0.9f, Screen.height * 0.7f);
        Vector3 ModelPos = m_cam.ScreenToWorldPoint(new Vector3(ScreenPoint.x, ScreenPoint.y, ScreenPoint.z));
        Model.transform.position = ModelPos;                         
        #endif
        #endregion
    }
    Vector3 modelSize ;
    enum Touchmode
    {
        click,
        rotate,
        move,
        scale
    }
    Touchmode t0_touchmode;
    Vector2 t0_touchPos;
    float pressTime = 0;
    bool enableRot = true;
    void ARInput()
    {
        if(Input.touchCount == 0)
        {
            enableRot = true;
            pressTime = 0;
            t0_touchmode = Touchmode.click;
            TouchMoveImage.gameObject.SetActive(false);
            return;
        }

        Touch touchZero = Input.GetTouch(0);
        
        if(Model.activeInHierarchy)
        {   
            
            if(Input.touchCount == 2)
            {
                scaleMode();
                t0_touchmode = Touchmode.scale;
            }
            
            else if(Input.touchCount == 1)
            {
                if(touchZero.phase == TouchPhase.Began)
                {
                    t0_touchPos = touchZero.position;
                    t0_touchmode = Touchmode.click;
                    if (IsPointerOverUIObject())
                        enableRot = false;
                }
                

                if(Vector2.Distance(t0_touchPos, touchZero.position) >= 25f && t0_touchmode == Touchmode.click)
                {
                    t0_touchmode = Touchmode.rotate;
                }
                else if(t0_touchmode == Touchmode.click && Vector2.Distance(t0_touchPos, touchZero.position) < 25f)
                {
                    pressTime += Time.deltaTime;
                    if(pressTime > 0.15f)
                    {
                        t0_touchmode = Touchmode.move;
                    }
                }

                if(touchZero.phase == TouchPhase.Ended && t0_touchmode == Touchmode.click)
                {
                    Ray ray = m_cam.ScreenPointToRay(touchZero.position);
                    RaycastHit hit;
                    if(Physics.Raycast(ray, out hit))
                    {
                        if(hit.collider.CompareTag("Pet")  )
                        {
                            Hitinteractive(hit);
                        }
                    }
                }
                
                if(enableRot)
                {
                    if(t0_touchmode == Touchmode.rotate)
                    {
                        if(!modelAnimator.GetCurrentAnimatorStateInfo(0).IsTag("interactive"))
                            Model.transform.Rotate(0, -touchZero.deltaPosition.x * Time.deltaTime * 10 * rotIndex, 0);
                        
                    }
                    else if(t0_touchmode == Touchmode.move && m_PlaneManager.enabled == false)
                    {
                        TouchMoveImage.gameObject.SetActive(true);
                        TouchMoveImage.position = t0_touchPos;
                        Vector3 dir = (touchZero.position - t0_touchPos).normalized ;

                        float dist = Vector2.Distance(touchZero.position, t0_touchPos) / 20;
                        dist = Mathf.Clamp(dist, 0, 10);

                        dir.z = 0;
                        Vector3 ScreenPoint = m_cam.WorldToScreenPoint(Model.transform.position);

                        // if(ScreenPoint.x < Screen.width - Screen.width * 0.9f || ScreenPoint.x > Screen.width * 0.9f)
                        //     dir.x = 0;
                        // if(ScreenPoint.y < Screen.height - Screen.height * 0.9f || ScreenPoint.y >  Screen.height * 0.9f)
                        //     dir.y = 0;

                        ScreenPoint += dir * dist;
                        ScreenPoint.x = Mathf.Clamp(ScreenPoint.x, Screen.width - Screen.width * 0.9f, Screen.width * 0.9f);
                        ScreenPoint.y = Mathf.Clamp(ScreenPoint.y, Screen.height - Screen.height * 0.9f, Screen.height * 0.9f);
                        Vector3 ModelPos = m_cam.ScreenToWorldPoint(new Vector3(ScreenPoint.x, ScreenPoint.y, ScreenPoint.z));
                        Model.transform.position =  ModelPos;     
                        // Vector3 CamUp = m_cam.transform.up;
                        // Vector3 CamRight = m_cam.transform.right;
                        // Vector3 ModelPos = Model.transform.position + CamRight.normalized * dir.x * Time.deltaTime * dist * moveindex +
                        //                                                 CamUp.normalized * dir.y * Time.deltaTime * dist;
     
                    }
                }
            }
        }

        if(!Model.activeInHierarchy)
        {
            if(m_ARRaycast.Raycast(touchZero.position, hits, TrackableType.PlaneWithinPolygon))
            {
                Pose pose = hits[0].pose;
                Model.transform.parent = null;
                Model.transform.position = pose.position;
                
                m_PlaneManager.SetTrackablesActive(false);
                m_PlaneManager.enabled = false;
                Model.SetActive(true);
                if(m_tutorial.step == 1)
                {
                    m_tutorial.next(1);
                }
                return;
            }
        }
    }

    void CheckIsOutOgBound()
    {
        
        
    }
    void Hitinteractive(RaycastHit hit)
    {
        if(petCategory == 0)
        {
            // if(animationIndex == 0 && !HeartEffect.gameObject.activeSelf)
            // {
            //     HeartEffect.transform.position = hit.point;
            //     HeartEffect.gameObject.SetActive(true);

            // }
            if(!modelAnimator.GetCurrentAnimatorStateInfo(0).IsTag("interactive"))
            {
                modelAnimator.SetTrigger("Interactive");
                if(animationIndex == 2)
                {
                    m_petAnimCtrl.endAction = EndAction_DisableButterfly;
                    //InteractiveProp[0].gameObject.SetActive(true);
                }
                
            }
        }
        else
        {
            // if(animationIndex == 1 && !HeartEffect.gameObject.activeSelf)
            // {
            //     HeartEffect.transform.position = hit.point;
            //     HeartEffect.gameObject.SetActive(true);
            // }
            
            if(!modelAnimator.GetCurrentAnimatorStateInfo(0).IsTag("interactive"))
                modelAnimator.SetTrigger("Interactive");
        }
    }
    void scaleMode()
    {
        Touch touchOne = Input.GetTouch(1);
        Touch touchZero = Input.GetTouch(0);
        // Find the position in the previous frame of each touch.
        Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
        Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

        // Find the magnitude of the vector (the distance) between the touches in each frame.
        float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
        float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

        // Find the difference in the distances between each frame.
        float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;
        deltaMagnitudeDiff =deltaMagnitudeDiff * Time.deltaTime * 0.1f;

        Vector3 scaleSize = new Vector3(deltaMagnitudeDiff, deltaMagnitudeDiff, deltaMagnitudeDiff);
        modelSize = Model.transform.localScale;

        modelSize -= scaleSize;
        modelSize.x = Mathf.Clamp(modelSize.x, minScale, maxScale);
        modelSize.y = Mathf.Clamp(modelSize.y, minScale, maxScale);
        modelSize.z = Mathf.Clamp(modelSize.z, minScale, maxScale);

        
        Model.transform.localScale = Vector3.Lerp(Model.transform.localScale, modelSize, Time.deltaTime * 3);
    }
    public void EndAction_DisableButterfly()
    {
        m_petAnimCtrl.endAction = null;
        //InteractiveProp[0].gameObject.SetActive(false);
    }
    public void Reset()
    {
        m_PlaneManager.SetTrackablesActive(true);
        m_PlaneManager.enabled = true;
    }
    bool PlaneFound = false;
    void detectARPlane()
    {
        Vector3 screenCenter = m_cam.ViewportToScreenPoint(new Vector3(0.5f, 0.5f));
        m_ARRaycast.Raycast(screenCenter, hits , TrackableType.Planes);
        PlaneFound = hits.Count > 0;

        if(PlaneFound && m_tutorial.step == 0)
        {
            m_tutorial.next(0);
        }
    }
    private bool IsPointerOverUIObject() 
    {
        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }

    public void OrderAction(int index)
    {
        if(petCategory == 0)
            if(index == 1 && animationIndex ==4)
                modelAnimator.SetTrigger("Interactive");
        else
            if(index == 2 && animationIndex == 2)
                modelAnimator.SetTrigger("Interactive2");
            else if(index == 0 && animationIndex == 2)
                modelAnimator.SetTrigger("Walk");
    }
}
