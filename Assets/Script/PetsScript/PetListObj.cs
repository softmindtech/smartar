﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;


public class PetListObj : MonoBehaviour
{
 
    public class objData
    {
        public petCategory m_category;
        public int index;

        public objData(int category, int s_index)
        {
            m_category = (petCategory)category;
            index = s_index;
        }
    }
    public objData m_data;
    public Image m_PetPhoto;
    public Text NameAndVaritey;
    int Language;
    public Image BgImage;
    public Image CubeImage;
    public Image LockImage;
    public Sprite LockBgSprite;
    public void Lock()
    {
        BgImage.sprite = LockBgSprite;
        CubeImage.color = new Color(0.5f,0.5f,0.5f);
        LockImage.gameObject.SetActive(true);
    }
    public objData GetSelectPetData()
    {
        return m_data;
    }

    public void PetDetail(Sprite photo, string ref_name, string ref_variety)
    {

        
        m_PetPhoto.sprite = photo;
        NameAndVaritey.text = ref_name + "\n" + ref_variety;

    }
    
}
