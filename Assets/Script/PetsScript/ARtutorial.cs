﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARtutorial : MonoBehaviour
{

    Image[] tipsImage;

    // Start is called before the first frame update
    void Start()
    {
        tipsImage = GetComponentsInChildren<Image>();
        
    }
    public void ARCameraEnable()
    {
        step = 0;
        for (int i = 0; i < tipsImage.Length; i++)
        {
            tipsImage[i].color = i == 0 ? new Color(1,1,1,1) : new Color(1,1,1,0);
            tipsImage[i].gameObject.SetActive(i == 0 ? true : false);
        }
    }
    
    public int step = 0;

    public void next(int value)
    {
        if(step < tipsImage.Length && this.gameObject.activeSelf)
        {
            StartCoroutine(lerpSep(value));
            step ++;
        }
    }

    IEnumerator lerpSep(int index)
    {
        float eslape = 0;
        float lerpTime = 1;

        while(eslape < lerpTime)
        {
            tipsImage[index].color = Color.Lerp(tipsImage[index].color,new Color(1,1,1,0),eslape/lerpTime );
            eslape += Time.deltaTime;
            yield return new WaitForEndOfFrame();
        }

        tipsImage[index].gameObject.SetActive(false);

        if(index  <  tipsImage.Length - 1)
        {
            tipsImage[index + 1].gameObject.SetActive(true);
            eslape = 0;

            while(eslape < lerpTime)
            {
                tipsImage[index + 1].color = Color.Lerp(tipsImage[index + 1].color,new Color(1,1,1,1),eslape/lerpTime );
                eslape += Time.deltaTime;
                yield return new WaitForEndOfFrame();
            }

            if(index + 1 == tipsImage.Length -1)
            {
                yield return new WaitForSeconds(1.5f);
                StartCoroutine(lerpSep(index + 1));
            }
        }
    }
}
