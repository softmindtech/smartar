﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;

[System.Serializable]
public class UnLockJson
{ 
    public int Category;
    public int lockIndex;
    public UnLockJson(int _Category, int _lockIndex)
    {
        Category = _Category;
        lockIndex = _lockIndex;
    }
}

[System.Serializable]
public class UnLockJsonContainer
{
    public List<UnLockJson> UnLockData = new List<UnLockJson>();
    
}
public class PlayerUnLockJson : MonoBehaviour
{

    public static PlayerUnLockJson instance;
    UnLockJsonContainer m_UnLockJsonContainer = new UnLockJsonContainer();
    public UnLockJsonContainer UnLockDatas;
    void Start()
    {
        LoadJson();
    }
    // Start is called before the first frame update
    public void AddData(int unlockCategory, int unlockIndex)
    {
        if(UnLockDatas.UnLockData.Count != 0)
        {
            if(!CheckUnlock(unlockCategory, unlockIndex))
                UnLockDatas.UnLockData.Add(new UnLockJson(unlockCategory, unlockIndex));
        }
        else
        {
            UnLockDatas.UnLockData.Add(new UnLockJson(unlockCategory, unlockIndex));
        }
        SaveJson();
    }

    public bool CheckUnlock(int unlockCategory, int unlockIndex)
    {
        bool match = false;
        foreach(UnLockJson x in UnLockDatas.UnLockData)
        {
            if(unlockIndex == x.lockIndex && unlockCategory == x.Category)
            {
                match = true;
            }
        }
        return match;
    }
    public void SaveJson()
    {
        string json = JsonUtility.ToJson(UnLockDatas);
        WriteToFile("UnLockData", json);
        #if UNITY_EDITOR
        AssetDatabase.Refresh();
        #endif
        
    }

    void WriteToFile(string fileName, string json)
    {   

        string path;
        #if UNITY_EDITOR
        path = Application.dataPath + "/Resources/" + fileName + ".json";
        #elif UNITY_ANDROID || UNITY_IOS
        path = Application.persistentDataPath + "/UnLockData.json";
        #else
        path = Application.persistentDataPath + "/UnLockData.json";
        #endif
         
        FileStream filestream = new FileStream(path, FileMode.Create);
        using(StreamWriter writer = new StreamWriter(filestream))
        {
            writer.WriteLine(json);
        }
    }

    public void ClearData()
    {
        UnLockDatas.UnLockData.Clear();
        SaveJson();
    }


    public void LoadJson()
    {
        string path;
        #if UNITY_EDITOR
        path = Application.dataPath + "/Resources/UnLockData.json";
        #elif UNITY_ANDROID || UNITY_IOS
        path = Application.persistentDataPath + "/UnLockData.json";
        #else
        path = Application.persistentDataPath + "/UnLockData.json";
        #endif


        if(File.Exists(path))
        {
            string loadedJsonDataString = File.ReadAllText(path);
            if(loadedJsonDataString != null)
            {
                m_UnLockJsonContainer = JsonUtility.FromJson<UnLockJsonContainer>(loadedJsonDataString);
                UnLockDatas = m_UnLockJsonContainer;
            }
        }
        else
        {
            SaveJson();
        }
    }
    // Update is called once per frame

}
