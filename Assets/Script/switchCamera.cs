﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using System;

using NatSuite.Examples;
public class switchCamera : MonoBehaviour
{
    public bool cameraSwicth {get; private set;} // defualt camera
    Button m_btn;
    public GameObject m_ARSessionOrigin;
    public GameObject PetCamera_FontCam;
    public GameObject TutorialGroup;
    public Button FollowBtn;
    //public VirtualJoystick m_virtualJoystick;

    ARPlaneManager m_ARplane;
    ARPlaceOnPlane m_ARplace;

    ARFaceManager m_ARFace;
    SpwanSelectPet m_spwanSelectPet;
    ARtutorial m_Artutorial;
    
    public MiniCam FontCameraImage;
    Action resetButtonAction;
    public enum placemode
    {
        NonArmode = 0,
        Armode = 1
    }
    public placemode m_placemode = (placemode)0;
    void Awake()
    {
        cameraSwicth = false;
        m_ARplane = m_ARSessionOrigin.GetComponent<ARPlaneManager>();
        m_ARplace = m_ARSessionOrigin.GetComponent<ARPlaceOnPlane>();
        m_spwanSelectPet = m_ARSessionOrigin.GetComponent<SpwanSelectPet>();
        m_Artutorial = TutorialGroup.GetComponent<ARtutorial>();
        m_btn = GetComponent<Button>();
        m_ARFace = m_ARSessionOrigin.GetComponent<ARFaceManager>();
        m_ARplane.enabled = !cameraSwicth;
        // m_ARplane.enabled = false;
        //m_ARFace.enabled = true;
    }
    void Start()
    {
        m_btn.onClick.AddListener(switchCameraMode);
        resetButtonAction = m_spwanSelectPet.reset;
        m_spwanSelectPet.resetButton.onClick.AddListener( () => resetButtonAction());


    }

    public void changePlaceMode()
    {
        if(!cameraSwicth)
        {
            m_ARplace.Model.SetActive(false);
            m_spwanSelectPet.resetButton.onClick.RemoveAllListeners();

            bool b_placemode = m_placemode == placemode.NonArmode ? false : true;
            // true = AR mode
            // false = non AR mode
            m_ARplane.SetTrackablesActive(b_placemode);
            m_ARplane.enabled = b_placemode;
            // m_virtualJoystick.gameObject.SetActive(!b_placemode);
            m_ARplace.rotIndex =  1;
            m_ARplace.Model.transform.transform.parent = b_placemode ? null : PetCamera_FontCam.transform;
            
            TutorialGroup.SetActive(b_placemode);
            if(b_placemode)
            {
                resetButtonAction = m_spwanSelectPet.reset;
                Quaternion rot = Quaternion.Euler(0,180,0);
                m_ARplace.Model.transform.rotation = rot;
                
                m_Artutorial.ARCameraEnable();
            }
            else
            {
                resetButtonAction = ResetPetPos_Fontcamera;
                StartCoroutine(delay());
            }
            
            m_spwanSelectPet.resetButton.onClick.AddListener( () => resetButtonAction());
        }
    }
    public void switchCameraMode()
    {
        m_spwanSelectPet.resetButton.onClick.RemoveAllListeners();
        m_ARplace.Model.SetActive(false);

        cameraSwicth = !cameraSwicth;
        // cameraSwicth false == back cam
        // cameraSwicth true == font cam



        m_ARplace.moveindex = cameraSwicth  ? -1 : 1;
        FollowBtn.interactable = !cameraSwicth;

        #if UNITY_IOS
            FontCameraImage.SwitchFontCamera(cameraSwicth);
        #elif UNITY_ANDROID
            m_ARplace.rotIndex = cameraSwicth ? -1 : 1;
            m_ARFace.enabled = cameraSwicth;
        #endif

        bool b_placemode = m_placemode == placemode.NonArmode ? false : true;
        // true = AR mode
        // false = non AR mode
        //m_ARplace.rotIndex = b_placemode ? -1 : 1;
        
        m_ARplane.enabled = !b_placemode;


        bool isARmode = b_placemode ? cameraSwicth : true;
        // if non ar it will true
        // if Ar mode and font camare true
        // if Ar mode and back camare false
        //m_virtualJoystick.gameObject.SetActive(isARmode);

        
        m_ARplane.enabled = !isARmode;

        m_ARplane.SetTrackablesActive(!isARmode);
        TutorialGroup.SetActive(!isARmode);
        if(isARmode)
        {
            resetButtonAction = ResetPetPos_Fontcamera;
            m_ARplace.Model.transform.transform.parent = PetCamera_FontCam.transform;
            StartCoroutine(delay());
        }
        else
        {
            m_Artutorial.ARCameraEnable();
            resetButtonAction = m_spwanSelectPet.reset;
            m_ARplace.Model.transform.transform.parent = null;
            Quaternion rot = Quaternion.Euler(0,180,0);
            m_ARplace.Model.transform.rotation = rot;
        }
        m_spwanSelectPet.resetButton.onClick.AddListener( () => resetButtonAction());
    }

    public void ResetPetPos_Fontcamera()
    {
        m_ARplace.Model.transform.position = PetCamera_FontCam.transform.position +
                                            PetCamera_FontCam.transform.forward.normalized * 0.5f;
        m_ARplace.Model.transform.localPosition -= new Vector3(0,0.13f,0);
    }
    IEnumerator delay()
    {
        yield return new WaitForSeconds(0.1f);
        ResetPetPos_Fontcamera();
        Quaternion rot = Quaternion.Euler(0,180,0);
        m_ARplace.Model.transform.localRotation = rot;
        m_ARplace.Model.SetActive(true);
        
    }
}
