﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NatSuite.Examples;


public class ARSceneDevice : MonoBehaviour
{

    [Header("IOSDevice")]
    public GameObject MiniCamCanvas;
    public MiniCam m_minicam;


    // Start is called before the first frame update
    void Awake()
    {
        #if UNITY_ANDROID
        MiniCamCanvas.SetActive(false);
        m_minicam.enabled = false;

        #elif UNITY_IOS
        MiniCamCanvas.SetActive(true);
        m_minicam.enabled = true;
        #endif



    }

}
