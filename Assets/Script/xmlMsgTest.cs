﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEditor;

public class xmlMsgTest : MonoBehaviour
{
    Button m_btn;
    NotificationXML m_NotificationXML;
    public bool Reset = false;
    // Start is called before the first frame update
    void Start()
    {
        if(NotificationXML.instance != null)
            m_NotificationXML = NotificationXML.instance;


        m_btn = GetComponent<Button>();
        if(!Reset)
            m_btn.onClick.AddListener(AddMsg);
        else
            m_btn.onClick.AddListener(Clear);
    }

    void AddMsg()
    {

        m_NotificationXML.AddData("aaa", "10/12");
        #if UNITY_EDITOR
        AssetDatabase.Refresh();
        #endif
        StartCoroutine(delay());
    }

    void Clear()
    {
        m_NotificationXML.ClearData();
        PlayerPrefs.DeleteKey("NumbderOfNotification");
        StartCoroutine(delay());
    }

    IEnumerator delay()
    {
        yield return new WaitForSeconds(0.5f);
        
        SceneManager.LoadScene(0);
    }
}
