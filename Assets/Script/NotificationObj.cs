﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class NotificationObj : MonoBehaviour
{


    public Text TimeText;
    public Text ContentText;
    Button m_btn;

    public NotificationDetailCtrl m_NotificationDetailCtrl;

    void Awake()
    {
        m_btn = GetComponent<Button>();
    }
    public void Msg(Notification m_notification, SelectPetsManager m_manager, int Language)
    {
        //string charName = m_manager.m_petsCategory[1].m_Petsmanager[m_notification.CharacterUnlock % 10].name;
        TimeText.text = m_notification.time;
        m_btn.onClick.AddListener(EnableNotificationDetailCtrl);
        if(m_notification.CharacterUnlock == -999)
        {

            m_btn.onClick.AddListener(() => m_NotificationDetailCtrl.RedeemCode(m_notification.RedeemCode, Language));
            if(Language == 0)
                ContentText.text = "You get the RedeemCode <b>" + m_notification.RedeemCode + "</b>.";
            else
                ContentText.text = "您收到兌換代碼<b>" + m_notification.RedeemCode + "</b>。";
        }
        else
        {
            m_btn.onClick.AddListener(() => m_NotificationDetailCtrl.UnlockCharatcer(Language));
            if(Language == 0)
            {
                string charName = m_manager.m_petsCategory[1].m_Petsmanager[m_notification.CharacterUnlock].m_petDetail.Variety;
                ContentText.text = "You successfully unlocked a Hero <b>" + charName + "</b> of Arena of Valor.";
                
            }
            else
            {
                if(m_notification.CharacterUnlock < m_manager.m_petsCategory[1].m_Petsmanager.Length)
                {
                    string charName = m_manager.m_petsCategory[1].m_Petsmanager[m_notification.CharacterUnlock].m_petDetail.c_variety;
                    ContentText.text = "您已成功解鎖傳說對決英雄<b>" + charName + "</b>。";
                }
            }
        }
    }
    public void EnableNotificationDetailCtrl()
    {
        m_NotificationDetailCtrl.gameObject.SetActive(true);
    }
    
}
