﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

[System.Serializable]
public class Notification
{ 
    public int CharacterUnlock;
    public string RedeemCode;
    public string time;
    public Notification(int _CharacterUnlock, string _RedeemCode, string _time)
    {
        CharacterUnlock = _CharacterUnlock;
        RedeemCode = _RedeemCode;
        time = _time;
    }
    public bool CheckRepeatCharacterUnlock(int _CharacterUnlock, string _RedeemCode)
    {
        if(CharacterUnlock == _CharacterUnlock && RedeemCode == _RedeemCode)
            return true;
        
        return false;
    }
}



[System.Serializable]
public class NotificationContainer
{
    public List<Notification> notification = new List<Notification>();
    

    public bool CheckRepeatNotification(Notification n)
    {
        
        for (int i = 0; i < notification.Count; i++)
        {
            if(notification[i].CheckRepeatCharacterUnlock(n.CharacterUnlock, n.RedeemCode))
                return true;
        }
        return false;
    }
}


public class NotificationXML : MonoBehaviour
{

    public static NotificationXML instance;
    NotificationContainer m_NotificationContainer = new NotificationContainer();
    
    public NotificationContainer NotificationSave;
    public Action LoadEnd;

    void Awake()
    {
        if(instance == null)
            instance = this;
    }
    void Start()
    {
        LoadJson();
    }


    public void AddData(int CharacterUnlock, string Date)
    {
        // string localTime = System.DateTime.Now.Day + "/" + 
        //                     System.DateTime.Now.Month +  "/" + 
        //                     System.DateTime.Now.Year + " " +
        //                     System.DateTime.Now.Hour + ":" + 
        //                     System.DateTime.Now.Minute;
        if(Date == null || Date == string.Empty)
            Date = "";
        //string localTime = System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute;
        Notification addN = new Notification(CharacterUnlock, "null", Date);
        if(!CheckRepeat(addN))
        {
            NotificationSave.notification.Add(addN);
            SaveJson();
        }
    }

    bool CheckRepeat(Notification checkValue)
    {
        
        if(m_NotificationContainer.CheckRepeatNotification(checkValue))
            return true;
        if(NotificationSave.CheckRepeatNotification(checkValue))
            return true;

        return false;
    }
    public void AddData(string RedeemCode, string Date)
    {
        if(Date == null || Date == string.Empty)
            Date = "";
        //string localTime = System.DateTime.Now.Hour + ":" + System.DateTime.Now.Minute;

        Notification addN = new Notification(-999, RedeemCode, Date);
        if(!CheckRepeat(addN))
        {
            NotificationSave.notification.Add(addN);
            SaveJson();
        }
    }
    public void SaveJson()
    {
        string json = JsonUtility.ToJson(NotificationSave);
        WriteToFile("Notification", json);
        
    }

    public void ClearData()
    {
        NotificationSave.notification.Clear();
        SaveJson();
    }

    void WriteToFile(string fileName, string json)
    {   

        string path;
        #if UNITY_EDITOR
        path = Application.dataPath + "/Resources/" + fileName + ".json";
        #elif UNITY_ANDROID || UNITY_IOS
        path = Application.persistentDataPath + "/Notification.json";
        #else
        path = Application.persistentDataPath + "/Notification.json";
        #endif
         
        FileStream filestream = new FileStream(path, FileMode.Create);
        using(StreamWriter writer = new StreamWriter(filestream))
        {
            writer.WriteLine(json);
        }
    }
    public void LoadJson()
    {
        string path;
        #if UNITY_EDITOR
        path = Application.dataPath + "/Resources/Notification.json";
        #elif UNITY_ANDROID || UNITY_IOS
        path = Application.persistentDataPath + "/Notification.json";
        #else
        path = Application.persistentDataPath + "/Notification.json";
        #endif


        if(File.Exists(path))
        {
            string loadedJsonDataString = File.ReadAllText(path);
            if(loadedJsonDataString != null)
            {
                m_NotificationContainer = JsonUtility.FromJson<NotificationContainer>(loadedJsonDataString);
                NotificationSave = m_NotificationContainer;
            }
        }
        else
        {
            SaveJson();
        }

        if(LoadEnd != null)
            LoadEnd();
    }
}
