﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LockTest : MonoBehaviour
{
    public PetManager date; 
    PlayerUnLockJson m_playerUnlock;
    SelectPetsManager m_manager;
    // Start is called before the first frame update
    void Start()
    {
        if(SelectPetsManager.instance != null)
        {
            m_manager = SelectPetsManager.instance;
            m_playerUnlock = m_manager.GetComponent<PlayerUnLockJson>();
        }
            
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            NotificationXML.instance.AddData(0,"");
            SceneManager.LoadScene("Menu");
        }
        if(Input.GetKeyDown(KeyCode.R))
        {
            NotificationXML.instance.AddData(1,"");
            SceneManager.LoadScene("Menu");
        }
        if(Input.GetKeyDown(KeyCode.T))
        {
            NotificationXML.instance.AddData(2,"");
            SceneManager.LoadScene("Menu");
        }
        if(Input.GetKeyDown(KeyCode.Y))
        {
            NotificationXML.instance.AddData(3,"");
            SceneManager.LoadScene("Menu");
        }
        if(Input.GetKeyDown(KeyCode.Q))
        {
            NotificationXML.instance.AddData(4,"");
            SceneManager.LoadScene("Menu");
        }
    }


}
