﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationCtrl : MonoBehaviour
{

    SelectPetsManager m_manager;
    NotificationXML m_notificationXML;
    Image m_image;
    NotificationContainer m_notificationData;
    Button m_button;
    BottomBtnObj m_bottomBtmObj;
    public Sprite[] m_sprites;
    public Transform NotificationContainer;
    public Transform NotificationContainer_Empty;
    public GameObject m_NotificationPrefab;
    public NotificationDetailCtrl m_NotificationDetailCtrl;
    bool spwaned = false;
    bool newNotification = false;
    int NumbderOfNotification;

    int Language = 0;
    // Start is called before the first frame update
    void Start()
    {
        if(SelectPetsManager.instance != null)
            m_manager = SelectPetsManager.instance;

        m_notificationXML = m_manager.GetComponent<NotificationXML>();
        m_notificationXML.LoadJson();
        m_notificationData = m_notificationXML.NotificationSave;

        m_image = GetComponent<Image>();
        m_button = GetComponent<Button>();
        m_bottomBtmObj = GetComponent<BottomBtnObj>();
        m_button.onClick.AddListener(GenerateNotification);
        CheckNotification();


        if(PlayerPrefs.HasKey("Language"))
            Language = PlayerPrefs.GetInt("Language");
        else
            Language = 1;
    }

    void CheckNotification()
    {
        if(PlayerPrefs.HasKey("NumbderOfNotification"))
        {
            NumbderOfNotification = PlayerPrefs.GetInt("NumbderOfNotification");
            if(m_notificationData.notification.Count > NumbderOfNotification)
            {

                m_image.sprite = m_sprites[1];
                m_bottomBtmObj.ChageOrigin(m_sprites[1]);
                newNotification = true;
            }
            else
            {
                newNotification = false;
            }
        }
    }
    void GenerateNotification()
    {
        m_image.sprite = m_sprites[2];
        m_bottomBtmObj.ChageOrigin(m_sprites[0]);
        if(!spwaned)
        {
            if(m_notificationData.notification.Count != 0)
            {
                foreach(Notification n in m_notificationData.notification)
                {
                    spwan(n);
                }
                NotificationContainer_Empty.SetAsLastSibling();
                
            }
            spwaned = true;
        }
        else
        {
            if(newNotification)
            {
                int flag = 0;
                foreach(Notification n in m_notificationData.notification)
                {
                    if(flag > NumbderOfNotification)
                    {
                        spwan(n);
                    }
                    flag++;
                }
                NumbderOfNotification = m_notificationData.notification.Count;
                NotificationContainer_Empty.SetAsLastSibling();
                newNotification = false;
            }
            
        }
        PlayerPrefs.SetInt("NumbderOfNotification", m_notificationData.notification.Count);
    }

    void spwan(Notification n)
    {
        GameObject buffer = Instantiate(m_NotificationPrefab, transform.position, Quaternion.identity);
        buffer.transform.parent = NotificationContainer;
        buffer.transform.localScale = Vector3.one;
        NotificationObj m_bufferScript = buffer.GetComponent<NotificationObj>();
        m_bufferScript.m_NotificationDetailCtrl = m_NotificationDetailCtrl;
        m_bufferScript.Msg(n, m_manager, Language);
    }
}
