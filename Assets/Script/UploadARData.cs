﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NatSuite.Examples.Components;

public class UploadARData : MonoBehaviour
{
    
    SwitchRecordMode m_SwitchRecordMode;
    public SwicthPetPlaceMode m_SwicthPetPlaceMode;
    GAtrack m_GAtrack;
    RecordButton m_recordBtn;
    // Start is called before the first frame update
    void Start()
    {
        m_GAtrack = gameObject.GetComponentInParent<GAtrack>();
        m_recordBtn = GetComponent<RecordButton>();
        m_SwitchRecordMode = GetComponent<SwitchRecordMode>();
        m_recordBtn.m_onPressRelase += UploadData;
        m_SwitchRecordMode.m_upload += UploadData;
    }

    void UploadData()
    {
        string recordMode = m_SwitchRecordMode.swicthVideo ? "Video" : "Photo";
        string ARmode = m_SwicthPetPlaceMode.ARmode ? "ARmode" : "Fixmode";
  
        m_GAtrack.LogCapture(recordMode, ARmode);
    }
}
