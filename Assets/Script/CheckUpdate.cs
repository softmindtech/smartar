﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class CheckUpdate : MonoBehaviour
{
    public GameObject StartBtn;
    public GameObject updateMsg;
    public LoginBehaviour login;

    private string baseAddress = "https://smallminddemo.s3-ap-northeast-1.amazonaws.com/";
    private string redirectLink = "https://play.google.com/store/apps/details?id=com.Softmind.SmartAR&hl=en";


    void Start()
    {
        updateMsg.SetActive(false);

        StartBtn.SetActive(false);
       // StartCoroutine(GetVersion());

        if (PlayerPrefs.HasKey("userid"))
        {
            Debug.Log("load list");
            login.LoadListsGoMenu();
            Debug.Log("after load list");
            // SceneManager.LoadScene("Menu");
        }
        else
        {
            Debug.Log("show btn");
            StartBtn.SetActive(true);
        }
    }


    void Update()
    {
        
    }


    IEnumerator GetVersion ()
    {
       string address = baseAddress;

#if UNITY_IOS
        address = baseAddress + "IOSversion.txt";
        redirectLink = "https://www.smartone.com/hk/smartonear/download/iOS/tc/";

#else
        address = baseAddress + "AndroidVersion.txt";
    
#endif

        UnityWebRequest www = UnityWebRequest.Get(address);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(": Error: " + www.error);
        }
        else
        {

            float serverVersion = float.Parse(www.downloadHandler.text);
            float currentVersion = float.Parse(Application.version);
            Debug.Log("serverVersion " + serverVersion);
            Debug.Log("currentVersion " +currentVersion);

            if (serverVersion<=currentVersion)
            {
                Debug.Log("most update");
                //if logined
                
                if (PlayerPrefs.HasKey("userid"))
                {
                    Debug.Log("load list");
                    login.LoadListsGoMenu();
                    Debug.Log("after load list");
                    // SceneManager.LoadScene("Menu");
                }
                else
                {
                    Debug.Log("show btn");
                    StartBtn.SetActive(true);
                }
  
            }
            else
            {
                updateMsg.SetActive(true);
                StartBtn.SetActive(false);

                yield return new WaitForSeconds(4);
                Application.OpenURL(redirectLink);

            }
        }
    }
}
