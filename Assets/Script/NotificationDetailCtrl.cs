﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
public class NotificationDetailCtrl : MonoBehaviour
{

    public Text NotificationDetail_Text;
    public Action<int> NotificationPress;



    public void UnlockCharatcer(int Language)
    {
        if(Language == 0)
        {
            NotificationDetail_Text.text =  "You have successfully unlocked an “Arena of Valor” hero!" ;

        }
        else
        {
            NotificationDetail_Text.text =  "任務完成！你已經成功捕捉到《傳說對決》英雄！" ;
        }
    }

    public void RedeemCode(string code, int Language)
    {
        UniClipboard.SetText(code);
        if (Language == 0)
        {
            NotificationDetail_Text.text =  "The code is in clipboard. You can now redeem the mission reward with <b>" + code + "</b> in “AOV Gamergizer Quest” platform!\n\nGo to “AOV Gamergizer Quest” NOW:\nsmartone.com/hk/AOV/en" ;

        }
        else
        {
            NotificationDetail_Text.text =  "你可於「爆機傳說任務」活動平台，憑此「任務獎勵兌換碼」<b>" +  code + "</b>兌換任務獎勵！\n\n兌換碼已複製到剪貼簿。即去「爆機傳說任務」活動平台:\nsmartone.com/hk/AOV/tc";
        }
    }
}
