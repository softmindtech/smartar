﻿using UnityEngine;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif

public class GetLocationPermission : MonoBehaviour
{


    void Start()
    {
#if PLATFORM_ANDROID
        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            Permission.RequestUserPermission(Permission.FineLocation);

        }
#endif
    }


}