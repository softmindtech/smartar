﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ImaginationOverflow.UniversalDeepLinking;
using UnityEngine.SceneManagement;

public class LoginBehaviour : MonoBehaviour
{
    // private static string LOGIN_URL = "http://waptest.smartone.com/wmc/jsp/GameZone/jsp/smartarzone_login.jsp?lang=";
    private static string LOGIN_URL = "https://wap.smartone.com/wmc/jsp/GameZone/jsp/smartarzone_login.jsp?lang=";
    private static string USERID_KEY = "userid";

    public GameObject loginButton;
    public Text loginBtnText;
    public ApiController apiCtr;
    public PlayerUnLockJson unlockJson;

    private bool added = false;

    UniWebView webView;

    void Start()
    {
        // set total characters
        PlayerPrefs.SetInt("TotalChars", 15);
        // PlayerPrefs.SetInt("Language", 1);
        if (!PlayerPrefs.HasKey("Language"))
        {
            PlayerPrefs.SetInt("Language", 1);
        }


        int language = PlayerPrefs.GetInt("Language");
        if (language == 0)
        {
            language = 1;
        }
        else
        {
            language = 0;
        }

        LOGIN_URL = LOGIN_URL + language.ToString();

        if (language == 0)
        {
            loginBtnText.text = "開始";
        }
        else
        {
            loginBtnText.text = "Start";
        }

        // DeepLinkManager.Instance.LinkActivated += Instance_LinkActivated;

        loginButton.GetComponent<Button>().onClick.AddListener(() =>
        {

#if UNITY_EDITOR
            //   OnMessageFromLoading("_userid=074bbd2870849315acb6");
            OnMessageFromLoading("_userid=c420a857993dccfa9cb0");


#else
            if (webView==null)
            {
               
                webView = loginButton.AddComponent<UniWebView>();

                webView.Frame = new Rect(0, 0, Screen.width, Screen.height);
                webView.SetShowToolbarNavigationButtons(false);
                webView.Load(LOGIN_URL);
                webView.Show();

                webView.OnPageFinished += (view, statusCode, url) => {
                OnMessageFromLoading(url);
                };
            }
#endif

        });


    }

    private void Update()
    {

    }

    private void Instance_LinkActivated(LinkActivation linkActivation)
    {
        var url = linkActivation.Uri;
        Debug.Log("link activation: " + url);
        if (linkActivation.QueryString.ContainsKey("userid"))
        {
            string userId = linkActivation.QueryString["userid"];
            PlayerPrefs.SetString(USERID_KEY, userId);
            LoadListsGoMenu();
        }
    }


    public void OnMessageFromLoading(string msg)
    {
        Debug.Log("Msg" + msg);

      //  PlayerPrefs.SetInt("Language", 1);
        if (msg.Contains("userid="))
        {
            Destroy(webView);
            webView = null;
            loginButton.SetActive(false);

            string userId = "";
            int index = msg.IndexOf("userid=");
            if (index > 0)
                userId = msg.Substring(index + 7, msg.Length - index - 7);
            //userId = msg.Substring(index, msg.Length - index );

            Debug.Log("userId " + userId);
            PlayerPrefs.SetString(USERID_KEY, userId);

            // SceneManager.LoadScene("Menu");
            //load character and redeem list
            if (!added)
            {
                LoadListsGoMenu();
            }

        }
    }

    public void LoadListsGoMenu()
    {
        NotificationXML.instance.ClearData();
        int count = 0;

        // unlock first batch
        int limit = 15;
        for (int i = 0; i < limit; i++)
        {
            string key = i.ToString() + "lock";
            PlayerPrefs.SetInt(key, 1);
            //

            string date = "";
            //
            NotificationXML.instance.AddData(i, date);
            count++;
        }

        PlayerPrefs.SetInt("NumbderOfNotification", count);
        StartCoroutine(DelayLoadScene());
    }
    /*
    public void LoadListsGoMenu()
    {
        Debug.Log("before load list");
        apiCtr.GetCharacterList((ArrayList charIds) =>
        {
            NotificationXML.instance.ClearData();
            Debug.Log("#Tom " + charIds.Count);

            Debug.Log("#Tom1 ");
            int count = 0;

            // unlock first batch
            int limit = 15;
            for (int i = 0; i < limit; i++)
            {
                string key = i.ToString() + "lock";
                PlayerPrefs.SetInt(key, 1);
                //
                //
                string date = "";
                //
                //
                NotificationXML.instance.AddData(i, date);
                count++;
            }

            foreach (int id in charIds)
            {
                Debug.Log("#Tom2Loop " + id);
                if (id >= limit)
                {
                    //
                    //
                    string date = "";
                    //
                    //
                    NotificationXML.instance.AddData(id, date);

                    //PlayerPrefs.SetInt(id.ToString() + "lock", 1);
                    string key = id.ToString() + "lock";
                    PlayerPrefs.SetInt(key, 1);
                    count++;
                }

                if (id>=10 && id <= 12)
                {
                  //  NotificationXML.instance.AddData("aaaa", " ");
                }
            }

            PlayerPrefs.SetInt("NumbderOfNotification", count);
            Debug.Log("#Tom2 ");

            apiCtr.GetUserRedeemList((ArrayList codes) =>
            {
                added = true;
                Debug.Log("#Tom3 ");
                foreach (string code in codes)
                {
                    Debug.Log("#Tom3loop " + code);
                    //
                    //
                    string date = "";
                    //
                    //
                    NotificationXML.instance.AddData(code,date);

                }
                Debug.Log("#Tom4 gotRedeemList");

                StartCoroutine(DelayLoadScene());

                Debug.Log("#Tom5");
            });
        });
    }


    */

    IEnumerator DelayLoadScene()
    {
        yield return new WaitForSeconds(0.08f);
        SceneManager.LoadScene("Menu");
    }
}

