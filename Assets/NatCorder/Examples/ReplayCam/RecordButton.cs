﻿/* 
*   NatCorder
*   Copyright (c) 2020 Yusuf Olokoba
*/

namespace NatSuite.Examples.Components {

	using System.Collections;
	using UnityEngine;
	using UnityEngine.UI;
	using UnityEngine.Events;
	using UnityEngine.EventSystems;

	[RequireComponent(typeof(EventTrigger))]
	public class RecordButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

		public Image button, countdown;
		public UnityEvent onTouchDown, onTouchUp;
		private bool pressed;
		public Image RecordIcon;
		public Image BlockImage;
		public Sprite startRecord;
		public Sprite stopRecord;
		private const float MaxRecordingTime = 10f; // seconds
		public delegate void OnPressRelease();
		public OnPressRelease m_onPressRelase;
		private void Start () {
			
			Reset();
		}
		void OnDisable()
		{
			RecordIcon.enabled = false;
			BlockImage.enabled = false;
		}
		void OnEnable()
		{
			RecordIcon.sprite = startRecord;
			BlockImage.enabled = true;
			RecordIcon.enabled = true;
		}
		private void Reset () {
			// Reset fill amounts
			RecordIcon.sprite = startRecord;
			if (button)
				button.fillAmount = 1.0f;
			if (countdown)
				countdown.fillAmount = 0.0f;
		}

		void IPointerDownHandler.OnPointerDown (PointerEventData eventData) {
			// Start counting
			
		}

		void IPointerUpHandler.OnPointerUp (PointerEventData eventData) {
			// Reset pressed
			//pressed = false;
			pressed = !pressed;

			if(pressed)
			{
				RecordIcon.sprite = stopRecord;
				StartCoroutine(Countdown());
				
			}
			else
			{
				RecordIcon.sprite = startRecord;
				if(m_onPressRelase != null)
					m_onPressRelase();
			}
			
			

		}

		private IEnumerator Countdown () {
			
			// First wait a short time to make sure it's not a tap
			yield return new WaitForSeconds(0.2f);
			if (!pressed)
				yield break;
				
			// Start recording
			onTouchDown?.Invoke();
			// Animate the countdown
			float startTime = Time.time, ratio = 0f;
			while (pressed && (ratio = (Time.time - startTime) / MaxRecordingTime) < 1.0f) {
				countdown.fillAmount = ratio;
				button.fillAmount = 1f - ratio;
				yield return null;
			}
			// Reset
			Reset();
			// Stop recording
			onTouchUp?.Invoke();
		}
	}
}